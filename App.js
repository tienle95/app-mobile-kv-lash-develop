/* eslint-disable no-undef */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useLayoutEffect, useEffect } from 'react';

import { store } from './src/redux/configureStore';
import { Provider } from 'react-redux';
import Root from './src';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { persistor } from './src/redux/configureStore';
import { TextInput, Text, LogBox, Platform } from 'react-native';
import { initLanguage } from 'src/i18n';
import messaging from '@react-native-firebase/messaging';
import notifee, { EventType } from '@notifee/react-native';
import {
  handleClickNotification,
  onNotifeeMessageReceived
} from './src/services/shared/notification';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import ModalNotification from './src/components/Modal/ModalNotification';

Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;
TextInput.defaultProps = TextInput.defaultProps || {};
TextInput.defaultProps.allowFontScaling = false;

LogBox.ignoreAllLogs();
if (!__DEV__) {
  console.log(() => {});
}

const App = props => {
  useLayoutEffect(() => {
    init();
  }, []);

  async function requestUserPermission() {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log('Authorization status:', authStatus);
    }
  }

  useEffect(() => {
    (async () => {
      try {
        await requestUserPermission();
      } catch (error) {
        console.log('error', error);
      }
    })();
  }, []);

  useEffect(() => {
    const unsubscribe = () => {
      return notifee.onForegroundEvent(({ type, detail }) => {
        const { pressAction } = detail;
        switch (type) {
          case EventType.DISMISSED:
            notifee.cancelNotification(detail.notification.id);
            break;
          case EventType.PRESS:
            if (pressAction === 'mark_as_read') {
              console.log('mark as read');
            } else {
              handleClickNotification(detail.notification);
            }
            break;
          default:
            break;
        }
      });
    };
    unsubscribe();
  }, []);

  useEffect(() => {
    const unsubscribe = messaging().onMessage(async message => {
      await onNotifeeMessageReceived(message);
      console.log('onMessage', message);
    });

    return () => {
      unsubscribe();
    };
  }, []);

  useEffect(() => {
    messaging().onNotificationOpenedApp(async message => {
      console.log('onNotificationOpenedApp', message);
      handleClickNotification(message);
    });
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        console.log('getInitialNotification', remoteMessage);
        if (Platform.OS === 'android') {
          setTimeout(() => {
            handleClickNotification(remoteMessage);
          }, 100);
        } else {
          handleClickNotification(remoteMessage);
        }
      });
  }, []);

  const init = async () => {
    await initLanguage();
  };

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <GestureHandlerRootView style={{ flex: 1 }}>
          <Root {...props} />
          <ModalNotification />
        </GestureHandlerRootView>
      </PersistGate>
    </Provider>
  );
};

export default App;
