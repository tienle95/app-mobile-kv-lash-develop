module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./'],
        alias: {
          assets: './src/assets/',
          components: './src/components/',
          configs: './src/configs/',
          constants: './src/constants/',
          containers: './src/containers/',
          floow_sdk: './src/floow_sdk/',
          hook: './src/hook',
          i18n: './src/i18n',
          'redux-app': './src/redux/',
          routes: './src/routes/',
          services: './src/services/',
          utils: './src/utils/',
          helpers: './src/helpers'
        }
      }
    ],
    'react-native-reanimated/plugin'
  ]
};
