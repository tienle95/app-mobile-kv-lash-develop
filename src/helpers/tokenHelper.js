import AsyncStorage from '@react-native-async-storage/async-storage';
import { KEY_STORAGE } from 'constants/string';

export const setToken = async token => {
  await AsyncStorage.setItem(KEY_STORAGE.ACCESS_TOKEN, JSON.stringify(token));
};

export const getToken = async () => {
  const token = await AsyncStorage.getItem(KEY_STORAGE.ACCESS_TOKEN);
  return JSON.parse(token);
};

export const setIdToken = async idToken => {
  await AsyncStorage.setItem(KEY_STORAGE.ID_TOKEN, JSON.stringify(idToken));
};

export const getIdToken = async () => {
  const idToken = await AsyncStorage.getItem(KEY_STORAGE.ID_TOKEN);
  return JSON.parse(idToken);
};
