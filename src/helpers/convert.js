export const Base64PDFUri = uri => `data:application/pdf;base64,${uri}`;
