export const formatNumber = (x, coma) => {
  if (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, coma);
  } else {
    return 0;
  }
};
export const formatNumberNoZero = x => {
  if (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  } else {
    return '';
  }
};

export const formatString = x => {
  if (x) {
    return x.split(',').join('');
  } else {
    return '';
  }
};

export const removeUnuseZero = x => {
  if (!x) {
    return '';
  } else {
    while (x.length > 1 && x[0] === '0') {
      x = x.substr(1, x.length - 1);
    }
    return x;
  }
};

export const formatNumberPoint = (x, div = '.') => {
  if (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, div);
  } else {
    return 0;
  }
};

export const checkNumber = () => {
  const regNumber = new RegExp('^[0-9]+$');
  return regNumber;
};

/**
 * @param {string}
 * @return {string} with phone number
 */

export const formatPhoneNumber = phone => {
  return phone.toString().replace(/[^0-9]/g, '');
};

export const checkPhoneSingapore = phone => {
  if (phone === 0 || phone === '00' || phone === '0') {
    return false;
  }
  return true;
};

export const checkPhoneMaxMin = phone => {
  return !isNaN(phone?.trim()) && phone?.trim().length >= 5 && phone?.trim().length <= 20;
};

export const formatWallet = wallet => {
  if (wallet) {
    return wallet.replace(/\d{4}(?=.)/g, '$&  ');
  }
};
