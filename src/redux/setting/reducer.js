import { SETTINGS } from './constant';

const initialState = {
  lang: ''
};

const settingReducer = (state = initialState, action) => {
  switch (action.type) {
    case SETTINGS.CHANGE_LANGUAGE.HANDLER:
      console.log({ action });
      return { ...state, lang: action.lang };

    default:
      return state;
  }
};

export default settingReducer;
