import { actionTypes } from '../actionTypes';

export const app = 'SETTINGS';

export const SETTINGS = {
  CHANGE_LANGUAGE: actionTypes(app + '/CHANGE_LANGUAGE')
};
