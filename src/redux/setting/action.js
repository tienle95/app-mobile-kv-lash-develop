import { SETTINGS } from './constant';

export const changeLanguage = lang => {
  return {
    type: SETTINGS.CHANGE_LANGUAGE.HANDLER,
    lang
  };
};
