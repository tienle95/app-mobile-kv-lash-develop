import { LOGIN, LOGOUT } from './constants';

export const loginHandle = payload => ({
  type: LOGIN.HANDLER,
  payload
});

export const logoutHandle = payload => ({
  type: LOGOUT.HANDLER,
  payload
});
