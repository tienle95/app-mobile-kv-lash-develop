import { actionTypes } from 'redux-app/actionTypes';

export const LOGIN = actionTypes('auth/LOGIN');

export const LOGOUT = actionTypes('auth/LOGOUT');
