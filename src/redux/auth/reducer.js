import { LOGIN, LOGOUT } from './constants';

const initialState = {
  isLogin: false,
  accessToken: ''
};

const authReducer = (state = initialState, action) => {
  const { payload, type } = action;
  switch (type) {
    case LOGIN.HANDLER: {
      return {
        ...state,
        isLogin: true,
        accessToken: payload?.accessToken
      };
    }

    case LOGOUT.HANDLER: {
      return {
        ...state,
        isLogin: false,
        accessToken: initialState.accessToken
      };
    }

    default:
      return state;
  }
};

export default authReducer;
