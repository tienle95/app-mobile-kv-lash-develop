import { all, fork } from 'redux-saga/effects';
import candidateSagas from '../app/saga';
import notificationSaga from '../notification/sagas';

export default function* rootSaga() {
  yield all([fork(candidateSagas), fork(notificationSaga)]);
}
