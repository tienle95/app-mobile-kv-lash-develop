import {SAVE_PREVIOUS_CURRENT_ROUTE, SAVE_LAST_ROUTE_FNB} from './constants';

export const savePreviousCurrentRoute = (payload = {}) => ({
  type: SAVE_PREVIOUS_CURRENT_ROUTE,
  payload,
});

export const saveLastRouteFNB = payload => ({
  type: SAVE_LAST_ROUTE_FNB,
  payload,
});
