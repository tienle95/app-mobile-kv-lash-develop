import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware } from 'redux';
import { persistStore } from 'redux-persist';
import rootReducer from './reducer/rootReducer';
import { createLogger } from 'redux-logger';
import rootSaga from './saga/rootSaga';
let middlewares = [];
const sagaMiddleware = createSagaMiddleware();

const loggerMiddleware = createLogger({
  collapsed: (getState, action, logEntry) => !logEntry.error
});

middlewares = [...middlewares, loggerMiddleware, sagaMiddleware];

const middleware = applyMiddleware(...middlewares);

export const store = createStore(rootReducer, middleware);
export const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);
