import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import historyRoute from '../historyRoute/reducer';
import AsyncStorage from '@react-native-async-storage/async-storage';
import appReducer from 'redux-app/app/reducer';
import settingReducer from 'redux-app/setting/reducer';
import loadingReducer from 'redux-app/loading/reducer';
import authReducer from 'redux-app/auth/reducer';
import notificationReducer from '../notification/reducer';

const rootPersistConfig = {
  key: 'historyRoute',
  storage: AsyncStorage
};

const notificationPersistConfig = {
  key: 'notification',
  storage: AsyncStorage
};

const rootReducer = combineReducers({
  authReducer,
  appReducer,
  historyRoute: persistReducer(rootPersistConfig, historyRoute),
  notificationReducer: persistReducer(notificationPersistConfig, notificationReducer),
  settingReducer,
  loadingReducer
});

export default rootReducer;
