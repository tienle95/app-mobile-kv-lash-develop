import { actionTypes } from 'redux-app/actionTypes';

export const GET_NOTIFICATION = actionTypes('notification/GET_NOTIFICATION');

export const READ_NOTIFICATION = actionTypes('notification/READ_NOTIFICATION');

export const DELETE_NOTIFICATION = actionTypes('notification/DELETE_NOTIFICATION');

export const CLEAR_STATE = actionTypes('notification/CLEAR_STATE');

export const SEE_ALL_NOTIFICATION = actionTypes('notification/SEE_ALL_NOTIFICATION');

export const DELETE_ALL_NOTIFICATION = actionTypes('notification/DELETE_ALL_NOTIFICATION');

export const GET_NEWS = actionTypes('notification/GET_NEWS');

export const GET_DETAIL_NEW = actionTypes('notification/GET_DETAIL_NEW');
