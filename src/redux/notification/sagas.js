import { call, fork, put, takeLatest } from 'redux-saga/effects';
import {
  // deleteAllNotificationApi,
  // deleteNotificationApi,
  // getNewDetailApi,
  // getNewsApi,
  // readNotificationApi,
  // seeAllNotificationApi,
  getNotificationApi
} from '../../services/api/notification';
import {
  // deleteAllNotificationFailure,
  // deleteAllNotificationSuccess,
  // deleteNotificationFailure,
  // deleteNotificationSuccess,
  // getDetailNewFailure,
  // getDetailNewHandle,
  // getDetailNewSuccess,
  // getNewsFailure,
  // getNewsSuccess,
  // readNotificationFailure,
  // readNotificationSuccess,
  // seeAllNotificationFailure,
  // seeAllNotificationSuccess,
  getNotificationFailure,
  getNotificationSuccess
} from './actions';
import { GET_NOTIFICATION } from './constants';
// import {
//   DELETE_ALL_NOTIFICATION,
//   DELETE_NOTIFICATION,
//   GET_DETAIL_NEW,
//   GET_NEWS,
//   GET_NOTIFICATION,
//   READ_NOTIFICATION,
//   SEE_ALL_NOTIFICATION
// } from './constants';

export function* getNotificationSaga(obj) {
  const { payload, onSuccess, onFailure } = obj;
  try {
    const res = yield call(getNotificationApi, payload);
    if (res?.status === 200) {
      yield put(getNotificationSuccess(res?.data));
      onSuccess?.(res?.data);
    } else {
      yield put(getNotificationFailure(res?.data));
      onFailure?.(res?.data);
    }
  } catch (error) {
    yield put(getNotificationFailure(error));
    onFailure?.(error);
  }
}

// export function* readNotificationSaga(obj) {
//   const { payload, onSuccess, onFailure } = obj;
//   try {
//     const res = yield call(readNotificationApi, payload);
//     if (res?.code === 200) {
//       yield put(readNotificationSuccess(payload));
//       onSuccess?.(res?.data);
//     } else {
//       yield put(readNotificationFailure(res?.data));
//       onFailure?.(res?.data);
//     }
//   } catch (error) {
//     yield put(readNotificationFailure(error));
//     onFailure?.(error);
//   }
// }

// export function* deleteNotificationSaga(obj) {
//   const { payload, onSuccess, onFailure } = obj;
//   try {
//     const res = yield call(deleteNotificationApi, payload);
//     if (res?.code === 200) {
//       yield put(deleteNotificationSuccess(res?.data));
//       onSuccess?.(res?.data);
//     } else {
//       yield put(deleteNotificationFailure(res?.data));
//       onFailure?.(res?.data);
//     }
//   } catch (error) {
//     yield put(deleteNotificationFailure(error));
//     onFailure?.(error);
//   }
// }

// export function* seeAllNotificationSaga(obj) {
//   const { onSuccess, onFailure } = obj;
//   try {
//     const res = yield call(seeAllNotificationApi);
//     if (res?.code === 200) {
//       yield put(seeAllNotificationSuccess(res?.data));
//       onSuccess?.(res?.data);
//     } else {
//       yield put(seeAllNotificationFailure(res?.data));
//       onFailure?.(res?.data);
//     }
//   } catch (error) {
//     yield put(seeAllNotificationFailure(error));
//     onFailure?.(error);
//   }
// }

// export function* deleteAllNotificationSaga(obj) {
//   const { onSuccess, onFailure } = obj;
//   try {
//     const res = yield call(deleteAllNotificationApi);
//     if (res?.code === 200) {
//       yield put(deleteAllNotificationSuccess(res?.data));
//       onSuccess?.(res?.data);
//     } else {
//       yield put(deleteAllNotificationFailure(res?.data));
//       onFailure?.(res?.data);
//     }
//   } catch (error) {
//     yield put(deleteAllNotificationFailure(error));
//     onFailure?.(error);
//   }
// }

// export function* getNewSaga(obj) {
//   const { payload, onSuccess, onFailure } = obj;
//   try {
//     const res = yield call(getNewsApi, payload);
//     if (res?.code === 200) {
//       yield put(getNewsSuccess(res?.data));
//       onSuccess?.(res?.data);
//     } else {
//       yield put(getNewsFailure(res?.data));
//       onFailure?.(res?.data);
//     }
//   } catch (error) {
//     yield put(getNewsFailure(error));
//     onFailure?.(error);
//   }
// }

// export function* getNewDetailSaga(obj) {
//   const { payload, onSuccess, onFailure } = obj;
//   try {
//     const res = yield call(getNewDetailApi, payload);
//     if (res?.code === 200) {
//       yield put(getDetailNewSuccess(res?.data));
//       onSuccess?.(res?.data);
//       onFailure?.(res?.data);
//     } else {
//       yield put(getDetailNewFailure(res?.data));
//     }
//   } catch (error) {
//     yield put(getDetailNewFailure(error));
//     onFailure?.(error);
//   }
// }

function* watchNotification() {
  yield takeLatest(GET_NOTIFICATION.HANDLER, getNotificationSaga);
  // yield takeLatest(DELETE_NOTIFICATION.HANDLER, deleteNotificationSaga);
  // yield takeLatest(READ_NOTIFICATION.HANDLER, readNotificationSaga);
  // yield takeLatest(SEE_ALL_NOTIFICATION.HANDLER, seeAllNotificationSaga);
  // yield takeLatest(DELETE_ALL_NOTIFICATION.HANDLER, deleteAllNotificationSaga);
  // yield takeLatest(GET_NEWS.HANDLER, getNewSaga);
  // yield takeLatest(GET_DETAIL_NEW.HANDLER, getNewDetailSaga);
}

export default function* notificationSaga() {
  yield fork(watchNotification);
}
