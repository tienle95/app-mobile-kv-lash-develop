export const listNotificationSelector = state => state.notificationReducer.listNotifications;

export const getListNewSelector = state => state.notificationReducer.listNews;

export const getNewDetailSelector = state => state.notificationReducer.newDetail;
