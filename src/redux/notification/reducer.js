import {
  CLEAR_STATE,
  DELETE_ALL_NOTIFICATION,
  GET_DETAIL_NEW,
  GET_NEWS,
  GET_NOTIFICATION,
  READ_NOTIFICATION,
  SEE_ALL_NOTIFICATION
} from './constants';

const initialState = {
  listNotifications: [],
  listNews: [],
  newDetail: {}
};

const notificationReducer = (state = initialState, action) => {
  const { payload, type } = action;
  switch (type) {
    case GET_NOTIFICATION.SUCCESS: {
      const newList = payload?.map(item => ({ ...item, is_read: false }));
      return {
        ...state,
        listNotifications: newList
      };
    }

    case CLEAR_STATE.CLEAR: {
      return {
        ...state,
        [payload]: initialState[payload]
      };
    }

    case READ_NOTIFICATION.SUCCESS: {
      const newList = [...state.listNotifications];
      const index = state.listNotifications.findIndex(noti => noti?.id === payload?.id);
      if (index >= 0) {
        newList[index] = { ...newList[index], is_read: true };
      }
      return {
        ...state,
        listNotifications: newList
      };
    }

    case SEE_ALL_NOTIFICATION.SUCCESS: {
      return {
        ...state,
        listNotifications: Array.from(state.listNotifications).map(item => ({
          ...item,
          is_read: true
        }))
      };
    }

    case DELETE_ALL_NOTIFICATION.SUCCESS: {
      return {
        ...state,
        listNotifications: []
      };
    }

    case GET_NEWS.SUCCESS: {
      return {
        ...state,
        listNews: payload
      };
    }

    case GET_DETAIL_NEW.SUCCESS: {
      return {
        ...state,
        newDetail: payload
      };
    }

    default:
      return state;
  }
};

export default notificationReducer;
