import {
  CLEAR_STATE,
  DELETE_ALL_NOTIFICATION,
  DELETE_NOTIFICATION,
  GET_DETAIL_NEW,
  GET_NEWS,
  GET_NOTIFICATION,
  READ_NOTIFICATION,
  SEE_ALL_NOTIFICATION
} from './constants';

export const getNotificationHandle = (payload, onSuccess, onFailure) => ({
  type: GET_NOTIFICATION.HANDLER,
  payload,
  onSuccess,
  onFailure
});

export const getNotificationSuccess = payload => ({
  type: GET_NOTIFICATION.SUCCESS,
  payload
});

export const getNotificationFailure = payload => ({
  type: GET_NOTIFICATION.FAILURE,
  payload
});

export const deleteNotificationHandle = (payload, onSuccess, onFailure) => ({
  type: DELETE_NOTIFICATION.HANDLER,
  payload,
  onSuccess,
  onFailure
});

export const deleteNotificationSuccess = payload => ({
  type: DELETE_NOTIFICATION.SUCCESS,
  payload
});

export const deleteNotificationFailure = payload => ({
  type: DELETE_NOTIFICATION.FAILURE,
  payload
});

export const readNotificationHandle = (payload, onSuccess, onFailure) => ({
  type: READ_NOTIFICATION.HANDLER,
  payload,
  onSuccess,
  onFailure
});

export const readNotificationSuccess = payload => ({
  type: READ_NOTIFICATION.SUCCESS,
  payload
});

export const readNotificationFailure = payload => ({
  type: READ_NOTIFICATION.FAILURE,
  payload
});

export const clearState = payload => ({
  type: CLEAR_STATE.CLEAR,
  payload
});

export const seeAllNotificationHandle = (onSuccess, onFailure) => ({
  type: SEE_ALL_NOTIFICATION.HANDLER,
  onSuccess,
  onFailure
});

export const seeAllNotificationSuccess = payload => ({
  type: SEE_ALL_NOTIFICATION.SUCCESS,
  payload
});

export const seeAllNotificationFailure = payload => ({
  type: SEE_ALL_NOTIFICATION.FAILURE,
  payload
});

export const deleteAllNotificationHandle = (onSuccess, onFailure) => ({
  type: DELETE_ALL_NOTIFICATION.HANDLER,
  onSuccess,
  onFailure
});

export const deleteAllNotificationSuccess = payload => ({
  type: DELETE_ALL_NOTIFICATION.SUCCESS,
  payload
});

export const deleteAllNotificationFailure = payload => ({
  type: DELETE_ALL_NOTIFICATION.FAILURE,
  payload
});

export const getDetailNewHandle = (payload, onSuccess, onFailure) => ({
  type: GET_DETAIL_NEW.HANDLER,
  payload,
  onSuccess,
  onFailure
});

export const getDetailNewSuccess = payload => ({
  type: GET_DETAIL_NEW.SUCCESS,
  payload
});

export const getDetailNewFailure = payload => ({
  type: GET_DETAIL_NEW.FAILURE,
  payload
});

export const getNewsHandle = (payload, onSuccess, onFailure) => ({
  type: GET_NEWS.HANDLER,
  payload,
  onSuccess,
  onFailure
});

export const getNewsSuccess = payload => ({
  type: GET_NEWS.SUCCESS,
  payload
});

export const getNewsFailure = payload => ({
  type: GET_NEWS.FAILURE,
  payload
});
