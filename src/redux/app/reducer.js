import {
  GET_APPOINTMENT,
  GET_BOOKING,
  GET_BRANCH,
  GET_CONTACT,
  GET_HOME,
  GET_INTRODUCE,
  GET_LIST_CANDIDATE,
  GET_POPUP_PROMOTION,
  SAVE_FCM_TOKEN
} from './constants';

const initialState = {
  listCandidate: [],
  homeData: {},
  booking: [],
  appointment: [],
  contact: [],
  introduce: [],
  branch: [],
  popupPromotion: [],
  fcmToken: ''
};

const appReducer = (state = initialState, action) => {
  const { payload, type } = action;

  switch (type) {
    case GET_LIST_CANDIDATE.HANDLER: {
      return {
        ...state
      };
    }

    case GET_LIST_CANDIDATE.SUCCESS: {
      return {
        ...state,
        listCandidate: payload
      };
    }

    case GET_LIST_CANDIDATE.FAILURE: {
      return {
        ...state,
        listCandidate: initialState.listCandidate
      };
    }

    case GET_LIST_CANDIDATE.CLEAR: {
      return {
        ...state,
        listCandidate: initialState.listCandidate
      };
    }

    case GET_HOME.SUCCESS: {
      return {
        ...state,
        homeData: payload
      };
    }

    case GET_HOME.FAILURE: {
      return {
        ...state,
        homeData: initialState.homeData
      };
    }

    case GET_HOME.CLEAR: {
      return {
        ...state,
        homeData: initialState.homeData,
        introduce: initialState.introduce,
        popupPromotion: initialState.popupPromotion
      };
    }

    case GET_INTRODUCE.SUCCESS: {
      return {
        ...state,
        introduce: payload
      };
    }

    case GET_INTRODUCE.FAILURE: {
      return {
        ...state,
        introduce: initialState.introduce
      };
    }

    case GET_BOOKING.SUCCESS: {
      return {
        ...state,
        booking: payload
      };
    }

    case GET_BOOKING.FAILURE: {
      return {
        ...state,
        booking: initialState.booking
      };
    }

    case GET_BOOKING.CLEAR: {
      return {
        ...state,
        booking: initialState.booking
      };
    }

    case GET_APPOINTMENT.SUCCESS: {
      return {
        ...state,
        appointment: payload
      };
    }

    case GET_APPOINTMENT.FAILURE: {
      return {
        ...state,
        appointment: initialState.appointment
      };
    }

    case GET_APPOINTMENT.CLEAR: {
      return {
        ...state,
        appointment: initialState.appointment
      };
    }

    case GET_CONTACT.SUCCESS: {
      return {
        ...state,
        contact: payload
      };
    }

    case GET_CONTACT.FAILURE: {
      return {
        ...state,
        contact: initialState.contact
      };
    }

    case GET_CONTACT.CLEAR: {
      return {
        ...state,
        contact: initialState.contact
      };
    }

    case GET_BRANCH.SUCCESS: {
      return {
        ...state,
        branch: payload
      };
    }

    case GET_BRANCH.FAILURE: {
      return {
        ...state,
        branch: initialState.branch
      };
    }

    case GET_BRANCH.CLEAR: {
      return {
        ...state,
        branch: initialState.branch
      };
    }

    case GET_POPUP_PROMOTION.SUCCESS: {
      return {
        ...state,
        popupPromotion: payload
      };
    }

    case GET_POPUP_PROMOTION.FAILURE: {
      return {
        ...state,
        popupPromotion: initialState.popupPromotion
      };
    }

    case GET_POPUP_PROMOTION.CLEAR: {
      return {
        ...state,
        popupPromotion: initialState.popupPromotion
      };
    }

    case SAVE_FCM_TOKEN.SUCCESS: {
      return {
        ...state,
        fcmToken: payload
      };
    }

    default:
      return state;
  }
};

export default appReducer;
