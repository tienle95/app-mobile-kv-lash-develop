import {
  BOOKING,
  GET_APPOINTMENT,
  GET_BOOKING,
  GET_BRANCH,
  GET_CONTACT,
  GET_HOME,
  GET_INTRODUCE,
  GET_LIST_CANDIDATE,
  GET_POPUP_PROMOTION,
  SAVE_FCM_TOKEN
} from './constants';

export const getListCandidateHandler = (payload, onSuccess, onFailed) => {
  return {
    type: GET_LIST_CANDIDATE.HANDLER,
    payload,
    onSuccess,
    onFailed
  };
};

export const getListCandidateSuccess = payload => {
  return {
    type: GET_LIST_CANDIDATE.SUCCESS,
    payload
  };
};

export const getListCandidateFailed = payload => {
  return {
    type: GET_LIST_CANDIDATE.FAILURE,
    payload
  };
};

export const clearListCandidate = payload => {
  return {
    type: GET_LIST_CANDIDATE.CLEAR,
    payload
  };
};

export const getAppHomeHandler = (payload, onSuccess, onFailed) => {
  return {
    type: GET_HOME.HANDLER,
    payload,
    onSuccess,
    onFailed
  };
};

export const getAppHomeSuccess = payload => {
  return {
    type: GET_HOME.SUCCESS,
    payload
  };
};

export const getAppHomeFailed = payload => {
  return {
    type: GET_HOME.FAILURE,
    payload
  };
};

export const getHomeClear = payload => {
  return {
    type: GET_HOME.CLEAR,
    payload
  };
};

export const getBookingHandler = (payload, onSuccess, onFailed) => {
  return {
    type: GET_BOOKING.HANDLER,
    payload,
    onSuccess,
    onFailed
  };
};

export const getBookingSuccess = payload => {
  return {
    type: GET_BOOKING.SUCCESS,
    payload
  };
};

export const getBookingFailed = payload => {
  return {
    type: GET_BOOKING.FAILURE,
    payload
  };
};

export const getBookingClear = payload => {
  return {
    type: GET_BOOKING.CLEAR,
    payload
  };
};

export const getAppointmentHandler = (payload, onSuccess, onFailed) => {
  return {
    type: GET_APPOINTMENT.HANDLER,
    payload,
    onSuccess,
    onFailed
  };
};

export const getAppointmentSuccess = payload => {
  return {
    type: GET_APPOINTMENT.SUCCESS,
    payload
  };
};

export const getAppointmentFailed = payload => {
  return {
    type: GET_APPOINTMENT.FAILURE,
    payload
  };
};

export const getAppointmentClear = payload => {
  return {
    type: GET_APPOINTMENT.CLEAR,
    payload
  };
};

export const getContactHandler = (payload, onSuccess, onFailed) => {
  return {
    type: GET_CONTACT.HANDLER,
    payload,
    onSuccess,
    onFailed
  };
};

export const getContactSuccess = payload => {
  return {
    type: GET_CONTACT.SUCCESS,
    payload
  };
};

export const getContactFailed = payload => {
  return {
    type: GET_CONTACT.FAILURE,
    payload
  };
};

export const getContactClear = payload => {
  return {
    type: GET_CONTACT.CLEAR,
    payload
  };
};

export const getIntroduceHandler = (payload, onSuccess, onFailed) => {
  return {
    type: GET_INTRODUCE.HANDLER,
    payload,
    onSuccess,
    onFailed
  };
};

export const getIntroduceSuccess = payload => {
  return {
    type: GET_INTRODUCE.SUCCESS,
    payload
  };
};

export const getIntroduceFailed = payload => {
  return {
    type: GET_INTRODUCE.FAILURE,
    payload
  };
};

export const getIntroduceClear = payload => {
  return {
    type: GET_INTRODUCE.CLEAR,
    payload
  };
};

export const getBranchHandler = (payload, onSuccess, onFailed) => {
  return {
    type: GET_BRANCH.HANDLER,
    payload,
    onSuccess,
    onFailed
  };
};

export const getBranchSuccess = payload => {
  return {
    type: GET_BRANCH.SUCCESS,
    payload
  };
};

export const getBranchFailed = payload => {
  return {
    type: GET_BRANCH.FAILURE,
    payload
  };
};

export const getBranchClear = payload => {
  return {
    type: GET_BRANCH.CLEAR,
    payload
  };
};

export const getPopupPromotionHandler = (payload, onSuccess, onFailed) => {
  return {
    type: GET_POPUP_PROMOTION.HANDLER,
    payload,
    onSuccess,
    onFailed
  };
};

export const getPopupPromotionSuccess = payload => {
  return {
    type: GET_POPUP_PROMOTION.SUCCESS,
    payload
  };
};

export const getPopupPromotionFailed = payload => {
  return {
    type: GET_POPUP_PROMOTION.FAILURE,
    payload
  };
};

export const clearPopupPromotion = payload => {
  return {
    type: GET_POPUP_PROMOTION.CLEAR,
    payload
  };
};

export const saveFcmTokenHandle = (payload, onSuccess, onFailed) => {
  return {
    type: SAVE_FCM_TOKEN.HANDLER,
    payload,
    onSuccess,
    onFailed
  };
};

export const saveFcmTokenSuccess = payload => {
  return {
    type: SAVE_FCM_TOKEN.SUCCESS,
    payload
  };
};

export const saveFcmTokenFailed = payload => {
  return {
    type: SAVE_FCM_TOKEN.FAILURE,
    payload
  };
};

export const bookingHandle = (payload, onSuccess, onFailed) => {
  return {
    type: BOOKING.HANDLER,
    payload,
    onSuccess,
    onFailed
  };
};

export const bookingSuccess = payload => {
  return {
    type: BOOKING.SUCCESS,
    payload
  };
};

export const bookingFailed = payload => {
  return {
    type: BOOKING.FAILED,
    payload
  };
};
