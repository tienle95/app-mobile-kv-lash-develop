export const getListCandidateSelector = state => state.appReducer.listCandidate;

export const getAppHomeSelector = state => state.appReducer.homeData;

export const getBookingSelector = state => state.appReducer.booking;

export const getAppointmentSelector = state => state.appReducer.appointment;

export const getContactSelector = state => state.appReducer.contact;

export const getIntroduceSelector = state => state.appReducer.introduce;

export const getBranchSelector = state => state.appReducer.branch;

export const getPopupPromotionSelector = state => state.appReducer.popupPromotion;
