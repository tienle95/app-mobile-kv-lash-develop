import { RESPONSE_CODE } from 'constants/network';
import moment from 'moment';
import { call, fork, put, takeLatest } from 'redux-saga/effects';
import {
  getAppointmentApi,
  getBookingApi,
  getBranchApi,
  getContactApi,
  getHomeApi,
  getIntroduceApi,
  getListCandidateApi,
  getPopupPromotionApi,
  submitBookingApi
} from 'services/api/candidate';
import { BRANCH_DATA } from 'utils/fakeData/branch';
import { BOOKING_DATA, CONTACT_DATA, HOME_DATA } from 'utils/fakeData/home';
import { showModal } from '../../components/Modal/ModalNotification';
import SCREENS from '../../constants/screens';
import NavigationServices from '../../utils/navigationServices';
import {
  bookingFailed,
  bookingSuccess,
  getAppHomeFailed,
  getAppHomeSuccess,
  getAppointmentFailed,
  getAppointmentSuccess,
  getBookingFailed,
  getBookingSuccess,
  getBranchFailed,
  getBranchSuccess,
  getContactFailed,
  getContactSuccess,
  getIntroduceFailed,
  getIntroduceSuccess,
  getListCandidateFailed,
  getListCandidateSuccess,
  getPopupPromotionFailed,
  getPopupPromotionSuccess
} from './actions';
import {
  BOOKING,
  GET_APPOINTMENT,
  GET_BOOKING,
  GET_BRANCH,
  GET_CONTACT,
  GET_HOME,
  GET_INTRODUCE,
  GET_LIST_CANDIDATE,
  GET_POPUP_PROMOTION
} from './constants';

export function* getListCandidateSaga(action) {
  const { payload, onSuccess, onFailed } = action;
  try {
    const res = yield call(getListCandidateApi, {
      params: payload
    });
    if (res?.status === RESPONSE_CODE.SUCCESS) {
      yield put(getListCandidateSuccess(res?.data?.data));
      onSuccess?.(res?.data?.data);
    } else {
      yield put(getListCandidateFailed(res));
      onFailed?.(res);
    }
  } catch (error) {
    yield put(getListCandidateFailed(error));
    onFailed?.(error);
  }
}

export function* getAppHomeSaga(action) {
  const { payload, onSuccess, onFailed } = action;
  try {
    const res = yield call(getHomeApi, {
      params: payload
    });
    // const res = HOME_DATA;
    if (res?.status === RESPONSE_CODE.SUCCESS) {
      yield put(getAppHomeSuccess(res?.data));
      onSuccess?.(res?.data);
    } else {
      yield put(getAppHomeFailed(res));
      onFailed?.(res);
    }
  } catch (error) {
    yield put(getAppHomeFailed(error));
    onFailed?.(error);
  }
}

export function* getBookingSaga(action) {
  const { payload, onSuccess, onFailed } = action;
  try {
    const res = yield call(getBookingApi, {
      params: payload
    });
    // const res = BOOKING_DATA;
    if (res?.status === RESPONSE_CODE.SUCCESS) {
      yield put(getBookingSuccess(res?.data));
      onSuccess?.(res?.data);
    } else {
      yield put(getBookingFailed(res));
      onFailed?.(res);
    }
  } catch (error) {
    yield put(getBookingFailed(error));
    onFailed?.(error);
  }
}

export function* getAppointmentSaga(action) {
  const { payload, onSuccess, onFailed } = action;
  try {
    const res = yield call(getAppointmentApi, {
      params: payload
    });
    // const res = BOOKING_DATA;
    if (res?.status === RESPONSE_CODE.SUCCESS) {
      yield put(getAppointmentSuccess(res?.data));
      onSuccess?.(res?.data);
    } else {
      yield put(getAppointmentFailed(res));
      onFailed?.(res);
    }
  } catch (error) {
    yield put(getAppointmentFailed(error));
    onFailed?.(error);
  }
}

export function* getContactSaga(action) {
  const { payload, onSuccess, onFailed } = action;
  try {
    const res = yield call(getContactApi, {
      params: payload
    });
    // const res = CONTACT_DATA;
    if (res?.status === RESPONSE_CODE.SUCCESS) {
      yield put(getContactSuccess(res?.data));
      onSuccess?.(res?.data);
    } else {
      yield put(getContactFailed(res));
      onFailed?.(res);
    }
  } catch (error) {
    yield put(getContactFailed(error));
    onFailed?.(error);
  }
}

export function* getIntroduceSaga(action) {
  const { payload, onSuccess, onFailed } = action;
  try {
    const res = yield call(getIntroduceApi, {
      params: payload
    });
    // const res = CONTACT_DATA;
    if (res?.status === RESPONSE_CODE.SUCCESS) {
      yield put(getIntroduceSuccess(res?.data));
      onSuccess?.(res?.data);
    } else {
      yield put(getIntroduceFailed(res));
      onFailed?.(res);
    }
  } catch (error) {
    yield put(getIntroduceFailed(error));
    onFailed?.(error);
  }
}

export function* getBranchSaga(action) {
  const { payload, onSuccess, onFailed } = action;
  try {
    const res = yield call(getBranchApi, {
      params: payload
    });
    // const res = BRANCH_DATA;

    if (res?.status === RESPONSE_CODE.SUCCESS) {
      yield put(getBranchSuccess(res?.data));
      onSuccess?.(res?.data);
    } else {
      yield put(getBranchFailed(res));
      onFailed?.(res);
    }
  } catch (error) {
    yield put(getBranchFailed(error));
    onFailed?.(error);
  }
}

export function* getPopupPromotionSaga(action) {
  const { payload, onSuccess, onFailed } = action;
  try {
    const res = yield call(getPopupPromotionApi, {
      params: payload
    });

    if (res?.status === RESPONSE_CODE.SUCCESS) {
      yield put(getPopupPromotionSuccess(res?.data));
      onSuccess?.(res?.data);
    } else {
      yield put(getPopupPromotionFailed(res));
      onFailed?.(res);
    }
  } catch (error) {
    yield put(getPopupPromotionFailed(error));
    onFailed?.(error);
  }
}

export function* bookingSaga(action) {
  const { payload, onSuccess, onFailed } = action;
  try {
    const res = yield call(submitBookingApi, {
      name: payload?.e?.name,
      phone_number: payload?.e?.phoneNumber,
      booking_date: new Date(
        moment(payload?.e?.bookingDate).format('YYYY/MM/DD') +
          ' ' +
          moment(payload?.e?.appointmentTime).format('HH:mm')
      ),
      branch_id: payload?.branch?.id,
      note: payload?.e?.note
    });
    if (res?.status === 200) {
      onSuccess?.(res);
      yield put(bookingSuccess(res));
      showModal({
        type: 'success',
        title: 'Successful',
        content: 'You have successfully submitted your booking request',
        onConfirm: () => NavigationServices.navigate(SCREENS.HOME)
      });
    }
  } catch (error) {
    showModal({
      type: 'error',
      title: 'Error',
      content: 'Something went wrong!'
    });
    onFailed?.(error);
    yield put(bookingFailed(error));
    console.log('error', error);
  }
}
function* watchUser() {
  yield takeLatest(GET_LIST_CANDIDATE.HANDLER, getListCandidateSaga);
  yield takeLatest(GET_HOME.HANDLER, getAppHomeSaga);
  yield takeLatest(GET_BOOKING.HANDLER, getBookingSaga);
  yield takeLatest(GET_APPOINTMENT.HANDLER, getAppointmentSaga);
  yield takeLatest(GET_CONTACT.HANDLER, getContactSaga);
  yield takeLatest(GET_INTRODUCE.HANDLER, getIntroduceSaga);
  yield takeLatest(GET_BRANCH.HANDLER, getBranchSaga);
  yield takeLatest(GET_POPUP_PROMOTION.HANDLER, getPopupPromotionSaga);
  yield takeLatest(BOOKING.HANDLER, bookingSaga);
}

export default function* rootChild() {
  yield fork(watchUser);
}
