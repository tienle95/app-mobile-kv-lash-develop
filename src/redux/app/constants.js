import { actionTypes } from 'redux-app/actionTypes';

export const GET_LIST_CANDIDATE = actionTypes('candidate/GET_LIST_CANDIDATE');

export const GET_HOME = actionTypes('app/GET_HOME');

export const GET_BOOKING = actionTypes('app/GET_BOOKING');

export const GET_APPOINTMENT = actionTypes('app/GET_APPOINTMENT');

export const GET_CONTACT = actionTypes('app/GET_CONTACT');

export const GET_INTRODUCE = actionTypes('app/GET_INTRODUCE');

export const GET_BRANCH = actionTypes('app/GET_BRANCH');

export const GET_POPUP_PROMOTION = actionTypes('app/GET_POPUP_PROMOTION');

export const SAVE_FCM_TOKEN = actionTypes('app/SAVE_FCM_TOKEN');

export const BOOKING = actionTypes('app/BOOKING');
