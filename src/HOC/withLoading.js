import React from 'react';
import { View } from 'react-native';
import { RootSiblingPortal } from 'react-native-root-siblings';
import { connect } from 'react-redux';
import AppLoading from '../components/AppLoading';
import { getLoadingSelector } from '../redux/loading/selector';

function withLoading(WrappedComponent, actionTypes) {
  function HOC({ isLoading, ...props }) {
    return (
      <View style={{ flex: 1 }}>
        <WrappedComponent {...props} />
        {isLoading && (
          <RootSiblingPortal>
            <View
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                alignItems: 'center',
                width: '100%',
                height: '100%',
                opacity: 0.5
              }}>
              <AppLoading />
            </View>
          </RootSiblingPortal>
        )}
      </View>
    );
  }
  const mapStateToProps = state => ({
    isLoading: getLoadingSelector(state, actionTypes)
  });
  return connect(mapStateToProps, null)(HOC);
}
export default withLoading;
