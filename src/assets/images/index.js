import avatarEmpty from './avatarEmpty.png';
import img_EmptyData from './img_emptyData.png';
import img_Header from './img_header.png';
import img_warning from './img_warning.png';
import img_success from './img_success.png';
import img_logoKvlash from './img_logoKvlash.png';
import img_empty from './empty.png';
import img_header_booking from './img_header_booking.png';

export {
  img_header_booking,
  avatarEmpty,
  img_EmptyData,
  img_Header,
  img_warning,
  img_success,
  img_logoKvlash,
  img_empty
};
