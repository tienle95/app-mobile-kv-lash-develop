import ic_back from './ic_back.png';
import ic_job from './ic_job.png';
import ic_note from './ic_note.png';
import ic_pen from './ic_pen.png';
import ic_position from './ic_position.png';
import ic_edit from './ic_edit.png';
import ic_eye from './ic_eye.png';
import ic_recChannel from './ic_recChannel.png';
import ic_carial from './ic_carial.png';
import back_icon from './back_icon.png';
import icon_payment_fail from './icon_payment_fail.png';
import icon_payment_success from './icon_payment_success.png';
import back_icon_right from './back_icon_right.png';
import warning_icon from './warning_icon.png';
import default_icon from './default_icon.png';
import ic_down from './ic_down.png';
import iconRadioActive from './iconRadioActive.png';
import iconRadioInActive from './iconRadioInActive.png';
import iconAddCard from './iconAddCard.png';
import iconBack from './iconBack.png';
import path from './path.png';
import iconCheckedVerify from './iconCheckedVerify.png';
import iconCheckVerify from './iconCheckVerify.png';
import iconAvailablePosition from './iconAvailablePosition.png';
import iconBookedPosition from './iconBookedPosition.png';
import iconYourChoicePosition from './iconYourChoicePosition.png';
import iconAttention from './iconAttention.png';
import iconTickWhite from './iconTickWhite.png';
import iconMail from './iconMail.png';
import iconContact from './iconContact.png';
import iconPhone from './iconPhone.png';
import ic_cancel from './ic_iconCancel.png';
import plusCircle from './ic_plusCircle.png';
import ic_iconEdit from './ic_iconEdit.png';
import ic_personInfo from './ic_personalInfo.png';
import ic_contactInfo from './ic_contactInfo.png';
import ic_copy from './ic_copy.png';
import ic_calendar from './ic_iconCalendar.png';
import ic_iconPen from './ic_pen.png';
import iconCheck from './ic_iconCheck.png';
import ic_delete from './ic_deleteOutlined.png';
import ic_skillAndLanguage from './ic_skillAndLanguage.png';
import ic_close from './ic_close.png';
import ic_education from './ic_education.png';
import ic_editGray from './ic_editGray.png';
import ic_certification from './ic_certification.png';
import ic_experience from './ic_experience.png';
import ic_cloudUpload from './ic_cloudUpload.png';
import ic_booking from './ic_booking.png';
import ic_hello from './ic_hello.png';
import ic_bell from './ic_bell.png';
import ic_read_all from './ic_read_all.png';
import ic_read_all_green from './ic_read_all_green.png';
import ic_read from './ic_read.png';
import ic_unread from './ic_unread.png';
import imgDefaultBranch from './imgDefaultBranch.png';
import ic_bookingWhite from './ic_bookingWhite.png';
import ic_arrowRight from './ic_arrowRight.png';
import iconSuccess from './iconSuccess.png';

export {
  iconSuccess,
  ic_arrowRight,
  ic_read,
  ic_unread,
  ic_read_all,
  ic_read_all_green,
  ic_bell,
  back_icon,
  icon_payment_fail,
  icon_payment_success,
  back_icon_right,
  warning_icon,
  default_icon,
  ic_down,
  iconRadioActive,
  iconRadioInActive,
  iconAddCard,
  iconBack,
  path,
  iconCheckedVerify,
  iconCheckVerify,
  iconAvailablePosition,
  iconBookedPosition,
  iconYourChoicePosition,
  iconAttention,
  iconTickWhite,
  iconMail,
  iconContact,
  iconPhone,
  plusCircle,
  ic_iconEdit,
  ic_personInfo,
  ic_contactInfo,
  ic_copy,
  ic_pen,
  iconCheck,
  ic_delete,
  ic_back,
  ic_job,
  ic_note,
  ic_iconPen,
  ic_position,
  ic_edit,
  ic_eye,
  ic_carial,
  ic_recChannel,
  ic_skillAndLanguage,
  ic_close,
  ic_education,
  ic_editGray,
  ic_certification,
  ic_cancel,
  ic_calendar,
  ic_experience,
  ic_cloudUpload,
  ic_booking,
  imgDefaultBranch,
  ic_hello,
  ic_bookingWhite
};
