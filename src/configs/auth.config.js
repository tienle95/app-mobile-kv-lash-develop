export const AppAuthConfig = {
  issuer: 'https://test.auth.okelaresume.com',
  clientId: 'akaREXSPA',
  redirectUrl: 'hireclite://action/login-callback',
  additionalParameters: { prompt: 'login' },
  scopes: [
    'openid',
    'profile',
    'akarex_profile',
    'akarex_api',
    'IdentityServerApi',
    'offline_access'
  ]
};

export const LogOutConfig = {
  issuer: 'https://test.auth.okelaresume.com'
};
