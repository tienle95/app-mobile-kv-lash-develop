import Config from 'react-native-config';

const config = {
  BASE_URL: Config.BASE_URL
};

const AppConfigs = config;

export default AppConfigs;
