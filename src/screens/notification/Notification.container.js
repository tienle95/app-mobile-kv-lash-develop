import { useIsFocused } from '@react-navigation/native';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getLoadingSelector } from 'redux-app/loading/selector';
import {
  clearState,
  deleteAllNotificationHandle,
  getNotificationHandle,
  readNotificationSuccess,
  seeAllNotificationSuccess
} from 'redux-app/notification/actions';
import {
  DELETE_ALL_NOTIFICATION,
  GET_NOTIFICATION,
  SEE_ALL_NOTIFICATION
} from 'redux-app/notification/constants';
import { listNotificationSelector } from 'redux-app/notification/selectors';
import SCREENS from '../../constants/screens';
import { seeAllNotificationHandle } from '../../redux/notification/actions';
import NavigationServices from '../../utils/navigationServices';
import NotificationView, { NOTIFICATION_TYPE } from './Notification.view';

const NOTIFICATION_ROUTER = {
  [NOTIFICATION_TYPE.news]: SCREENS.POST_DETAIL,
  [NOTIFICATION_TYPE.order_success]: SCREENS.ORDER_HISTORY_DETAIL,
  [NOTIFICATION_TYPE.update_order_status]: SCREENS.ORDER_HISTORY_DETAIL,
  [NOTIFICATION_TYPE.update_debt]: SCREENS.INFORMATION
};

const NotificationScreen = () => {
  const dispatch = useDispatch();
  const isFocused = useIsFocused();

  const loading = useSelector(state =>
    getLoadingSelector(state, [
      GET_NOTIFICATION.HANDLER,
      DELETE_ALL_NOTIFICATION.HANDLER,
      SEE_ALL_NOTIFICATION.HANDLER
    ])
  );

  const listNotifications = useSelector(listNotificationSelector);
  // const listNotifications = NOTIFICATION_DATA;

  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    getNotification();
  }, [getNotification]);

  useEffect(() => {
    return () => {
      dispatch(clearState('listNotifications'));
    };
  }, [dispatch]);

  const getNotification = useCallback(() => {
    dispatch(getNotificationHandle());
  }, [dispatch]);

  const goToDetail = useCallback(
    item => {
      NavigationServices.navigate(SCREENS.NOTIFICATION_DETAIL, { item });
      !item?.is_read && dispatch(readNotificationSuccess({ id: item?.id }));
    },
    [dispatch]
  );

  const readAll = useCallback(() => {
    if (Array.isArray(listNotifications) && listNotifications.length > 0) {
      dispatch(seeAllNotificationSuccess());
    }
  }, [listNotifications]);

  const seeAllNotification = () => {
    dispatch(
      seeAllNotificationHandle(
        () => {
          setIsVisible(false);
          getNotification();
        },
        () => setIsVisible(false)
      )
    );
  };

  const deleteAllNotification = () => {
    dispatch(
      deleteAllNotificationHandle(
        () => setIsVisible(false),
        () => setIsVisible(false)
      )
    );
  };
  return (
    <NotificationView
      data={listNotifications}
      isVisible={isVisible}
      setIsVisible={setIsVisible}
      loading={loading}
      goToDetail={goToDetail}
      deleteAllNotification={deleteAllNotification}
      getNotification={getNotification}
      seeAllNotification={seeAllNotification}
      readAll={readAll}
    />
  );
};

export default React.memo(NotificationScreen);
