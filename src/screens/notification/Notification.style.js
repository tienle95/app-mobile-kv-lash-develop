import { FONT_FAMILY } from 'constants/appFont';
import { colors } from 'constants/colors';
import { scale } from 'constants/scale';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
  },
  itemContainer: {
    paddingHorizontal: scale(12),
    paddingVertical: scale(8),
    backgroundColor: colors.white,
    borderRadius: scale(8),
    marginBottom: scale(14),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 2
  },
  itemHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: colors.sup_bright_gray,
    borderBottomWidth: 1,
    paddingBottom: scale(10)
  },
  headerText: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 1
  },
  itemThumb: {
    width: scale(28),
    height: scale(28),
    marginRight: scale(12)
  },
  icRead: {
    width: scale(16),
    height: scale(16)
  },
  itemBody: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginTop: scale(10)
  }
});
export default styles;
