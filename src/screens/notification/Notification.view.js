import AppText from 'components/AppText';
import EmptyContent from 'components/EmptyContent';
import { formatNumberPoint } from 'helpers/formatNumber';
import moment from 'moment';
import React, { memo, useCallback } from 'react';
import { useMemo } from 'react';
import { FlatList, SafeAreaView, TouchableHighlight, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import Modal from 'react-native-modal';
import {
  ic_read,
  ic_read_all,
  ic_unread,
  imgDefaultBranch,
  ic_read_all_green
} from '../../assets/icons';
import Header from '../../components/Header';
import StyledTouchable from '../../components/StyledTouchable';
import { colors } from '../../constants/colors';
import { scale } from '../../constants/scale';
import { EMPTY_STRING } from '../../constants/strings';
import styles from './Notification.style';

export const NOTIFICATION_TYPE = {
  update_order_status: 'update_order_status',
  /* {
    items_quantity: 'items_quantity',
    value: 'value'
  }, */
  update_debt: 'update_debt',
  /* {
    debt_remain: 'debt_remain',
    debt_volatility: 'debt_volatility'
  }, */
  news: 'news',
  /* {
    body: 'body'
  } */
  order_success: 'order_success'
};

const NotificationView = props => {
  const {
    data,
    goToDetail,
    isVisible,
    setIsVisible,
    deleteAllNotification,
    getNotification,
    seeAllNotification,
    readAll
  } = props;

  const rightIconHeader = useMemo(() => {
    if (data?.some(item => item?.is_read === false)) return ic_read_all_green;
    return ic_read_all;
  }, [data]);

  const renderContent = (type, _data) => {
    switch (type) {
      case NOTIFICATION_TYPE.update_debt:
        return (
          <>
            <AppText style={styles.itemContent}>
              Công nợ biến động: +{formatNumberPoint(_data?.debt_volatility)}đ
            </AppText>
            <AppText style={styles.itemContent}>
              Công nợ còn: {formatNumberPoint(_data?.debt_remain)}đ
            </AppText>
          </>
        );
      case NOTIFICATION_TYPE.order_success:
        return (
          <>
            <AppText style={styles.itemContent}>Số lượng: {_data?.items_quantity} sản phẩm</AppText>
            <AppText style={styles.itemContent}>
              Giá trị: {formatNumberPoint(_data?.value)}đ
            </AppText>
          </>
        );
      case NOTIFICATION_TYPE.update_order_status:
        return (
          <>
            <AppText style={styles.itemContent}>Đơn hàng: #{_data?.order_code}</AppText>
            <AppText style={styles.itemContent}>
              Giá trị: {formatNumberPoint(_data?.total_price)}đ
            </AppText>
          </>
        );
      case NOTIFICATION_TYPE.news:
        return <AppText style={styles.itemContent}>{_data?.body}</AppText>;
      default:
        return <></>;
    }
  };

  const renderItem = useCallback(
    ({ item }) => {
      return (
        <StyledTouchable style={styles.itemContainer} onPress={() => goToDetail(item)}>
          <View style={styles.itemHeader}>
            <FastImage
              source={item?.image ? { uri: item?.image } : imgDefaultBranch}
              style={styles.itemThumb}
              resizeMode="contain"
            />
            <View style={styles.headerText}>
              <AppText numberOfLines={1} size="L" weight="semi-bold">
                {item?.title?.length > 18
                  ? item?.title?.substring(0, 18) + '...'
                  : item?.title || EMPTY_STRING}
              </AppText>
              <AppText numberOfLines={1} size="XS" color={colors.text_gray} style={styles.itemDate}>
                {moment(item?.send_at).format('HH:mm - DD/MM/YYYY') || EMPTY_STRING}
              </AppText>
            </View>
          </View>
          <View style={styles.itemBody}>
            <FastImage style={styles.icRead} source={item?.is_read ? ic_read : ic_unread} />
            <AppText line={scale(22)} numberOfLines={2} ml-8 mr-16>
              {item?.subtitle}
            </AppText>
          </View>
        </StyledTouchable>
      );
    },
    [goToDetail]
  );

  const hideModal = () => setIsVisible(false);

  const renderEmpty = () => <EmptyContent />;

  return (
    <View style={styles.container}>
      <Modal isVisible={isVisible} animationIn={'fadeInUp'} animationOut={'fadeOutDown'}>
        <View style={{ position: 'absolute', bottom: 0, left: 0, right: 0 }}>
          <View style={styles.overBtnModal}>
            <TouchableHighlight underlayColor="#fff" onPress={seeAllNotification}>
              <AppText style={styles.titleBtn}>View all</AppText>
            </TouchableHighlight>
            <TouchableHighlight
              underlayColor={'#fff'}
              style={{ borderTopColor: '#D8D8D8', borderTopWidth: 1 }}
              onPress={deleteAllNotification}>
              <AppText style={styles.titleBtn}>Delete all</AppText>
            </TouchableHighlight>
          </View>

          <View style={[styles.overBtnModal, { marginTop: 8 }]}>
            <TouchableHighlight underlayColor={'#fff'} onPress={() => hideModal()}>
              <AppText style={styles.titleBtn} weight={'bold'}>
                Hủy
              </AppText>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
      {/* {loading && <Loading />} */}
      <SafeAreaView>
        <Header
          back
          title={'Notification'}
          rightIcon={rightIconHeader}
          onPressRightIcon={readAll}
          containerStyle={{ backgroundColor: '#00ADEF' }}
          titleColor={colors.white}
          leftIconStyle={{ tintColor: colors.white }}
          rightIconStyle={{ tintColor: colors.white }}
        />
      </SafeAreaView>

      <FlatList
        data={data}
        keyExtractor={(item, index) => `${item?.id}_${index}`}
        renderItem={renderItem}
        ListEmptyComponent={renderEmpty}
        contentContainerStyle={{ paddingVertical: 16, paddingHorizontal: scale(16) }}
        refreshing={false}
        onRefresh={() => getNotification()}
      />
    </View>
  );
};

export default memo(NotificationView);
