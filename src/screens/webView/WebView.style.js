import { colors } from 'constants/colors';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.light
  },
  contentContainer: {
    flex: 1,
    width: '100%',
    height: '100%'
  }
});
export default styles;
