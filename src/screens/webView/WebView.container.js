import React from 'react';
import WebViewScreen from './WebView.view';

const WebViewContainer = () => {
  return <WebViewScreen />;
};

export default WebViewContainer;
