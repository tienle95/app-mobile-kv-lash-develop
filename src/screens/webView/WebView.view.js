import { SafeAreaView, View } from 'react-native';
import React, { useState } from 'react';
import styles from './WebView.style';
import Header from 'components/Header';
import { WebView } from 'react-native-webview';
import { useRoute } from '@react-navigation/native';
import { img_logoKvlash } from 'assets/images';
import Loading from 'components/Loading';

const WebViewScreen = props => {
  const route = useRoute();
  const { link } = route.params || {};
  const [loading, setLoading] = useState(true);

  return (
    <View style={styles.container}>
      {loading && <Loading />}
      <SafeAreaView>
        <Header back logo={img_logoKvlash} />
      </SafeAreaView>

      <View style={styles.contentContainer}>
        <WebView
          source={{ uri: link }}
          onLoad={() => console.log('Load!!!')}
          onLoadStart={() => setLoading(true)}
          onLoadEnd={() => setLoading(false)}
          javaScriptEnabled={true}
        />
      </View>
    </View>
  );
};

export default WebViewScreen;
