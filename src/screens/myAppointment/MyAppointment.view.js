import { FlatList, SafeAreaView, View } from 'react-native';
import React, { useCallback } from 'react';
import styles from './MyAppointment.style';
import Header from 'components/Header';
import LinkButton from './components/LinkButton';
import Loading from 'components/Loading';

const MyAppointmentScreen = props => {
  const { data, loading, goToWebView } = props;

  const renderItem = useCallback(({ item }) => {
    return (
      <LinkButton
        title={item?.title}
        address={item?.address}
        onPress={() => goToWebView(item?.link)}
      />
    );
  }, []);

  return (
    <View style={styles.container}>
      {loading && <Loading />}
      <SafeAreaView>
        <Header back title={'MY APPOINTMENT'} />
      </SafeAreaView>

      <FlatList
        data={data}
        keyExtractor={item => item.id}
        renderItem={renderItem}
        ItemSeparatorComponent={<View style={styles.itemSeparatorComponent} />}
        style={styles.flatlistStyle}
      />
    </View>
  );
};

export default MyAppointmentScreen;
