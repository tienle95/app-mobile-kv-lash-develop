import React, { useCallback, useEffect } from 'react';
import MyAppointmentView from './MyAppointment.view';
import NavigationServices from '../../utils/navigationServices';
import SCREENS from '../../constants/screens';
import { useDispatch, useSelector } from 'react-redux';
import { getAppointmentSelector } from 'redux-app/app/selectors';
import { getAppointmentClear, getAppointmentHandler } from 'redux-app/app/actions';
import { getLoadingSelector } from 'redux-app/loading/selector';
import { GET_APPOINTMENT } from 'redux-app/app/constants';
import { openUrl } from 'utils/openUrl';

const MyAppointmentContainer = () => {
  const dispatch = useDispatch();

  const appointmentData = useSelector(getAppointmentSelector);
  const loading = useSelector(state => getLoadingSelector(state, [GET_APPOINTMENT.HANDLER]));

  useEffect(() => {
    dispatch(getAppointmentHandler());
  }, []);

  useEffect(() => {
    return () => {
      dispatch(getAppointmentClear());
    };
  }, []);

  const goToWebView = useCallback(url => {
    NavigationServices.navigate(SCREENS.WEB_VIEW, { link: url });
    // openUrl(url);
  }, []);

  return <MyAppointmentView data={appointmentData} loading={loading} goToWebView={goToWebView} />;
};

export default MyAppointmentContainer;
