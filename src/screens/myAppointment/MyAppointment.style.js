import { FONT_FAMILY } from 'constants/appFont';
import { colors } from 'constants/colors';
import { scale } from 'constants/scale';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.light
  },
  text: {
    fontSize: scale(20),
    alignSelf: 'center',
    color: colors.light
    // fontFamily: FONT_FAMILY.REGULAR
  },
  button: {
    margin: 10,
    borderRadius: 10,
    padding: 10,
    backgroundColor: 'blue'
  },
  titleHeader: {
    fontSize: scale(18),
    color: colors.dark
    // fontFamily: FONT_FAMILY.REGULAR
  },
  itemSeparatorComponent: { height: scale(8) },
  flatlistStyle: { paddingHorizontal: scale(18) },
  scroll: {
    paddingBottom: scale(32)
  }
});
export default styles;
