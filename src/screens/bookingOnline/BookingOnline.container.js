import React, { useCallback, useEffect } from 'react';
import BookingOnlineView from './BookingOnline.view';
import NavigationServices from '../../utils/navigationServices';
import SCREENS from '../../constants/screens';
import { useDispatch, useSelector } from 'react-redux';
import { getBookingSelector } from 'redux-app/app/selectors';
import { getBookingClear, getBookingHandler } from 'redux-app/app/actions';
import { getLoadingSelector } from 'redux-app/loading/selector';
import { GET_BOOKING } from 'redux-app/app/constants';
import { openUrl } from 'utils/openUrl';

const BookingOnlineContainer = () => {
  const dispatch = useDispatch();

  const bookingData = useSelector(getBookingSelector);
  const loading = useSelector(state => getLoadingSelector(state, [GET_BOOKING.HANDLER]));

  useEffect(() => {
    dispatch(getBookingHandler());
  }, []);

  useEffect(() => {
    return () => {
      dispatch(getBookingClear());
    };
  }, []);

  const goToWebView = useCallback(url => {
    NavigationServices.navigate(SCREENS.WEB_VIEW, { link: url });
    // openUrl(url);
  }, []);

  return <BookingOnlineView data={bookingData} loading={loading} goToWebView={goToWebView} />;
};

export default BookingOnlineContainer;
