import { FlatList, SafeAreaView, View } from 'react-native';
import React, { useCallback } from 'react';
import styles from './BookingOnline.style';
import Header from 'components/Header';
import LinkButton from './components/LinkButton';
import Loading from 'components/Loading';

const BookingOnlineScreen = props => {
  const { data, loading, goToWebView } = props;

  const renderItem = useCallback(({ item }) => {
    return (
      <LinkButton
        title={item?.title}
        address={item?.address}
        phone={item?.phone}
        onPress={() => goToWebView(item?.link)}
      />
    );
  }, []);

  return (
    <View style={styles.container}>
      {loading && <Loading />}
      <SafeAreaView>
        <Header back title={'BOOKING ONLINE'} />
      </SafeAreaView>

      <FlatList
        data={data}
        keyExtractor={(item, index) => `${item?.id}_${index}`}
        renderItem={renderItem}
        ItemSeparatorComponent={<View style={styles.itemSeparatorComponent} />}
        style={styles.flatlistStyle}
      />
    </View>
  );
};

export default BookingOnlineScreen;
