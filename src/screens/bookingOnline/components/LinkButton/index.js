import { TouchableOpacity } from 'react-native';
import React from 'react';
import styles from './styles';
import AppText from 'components/AppText';
import { EMPTY_STRING } from 'constants/strings';

const LinkButton = ({
  containerStyle,
  title,
  titleStyle,
  address,
  addressStyle,
  phone,
  phoneStyle,
  onPress,
  ...props
}) => {
  return (
    <TouchableOpacity style={[styles.container, containerStyle]} onPress={onPress} {...props}>
      <AppText style={[styles.title, titleStyle]}>{title || EMPTY_STRING}</AppText>
      <AppText style={[styles.address, addressStyle]}>{address || EMPTY_STRING}</AppText>
      <AppText style={[styles.phone, phoneStyle]}>{phone || EMPTY_STRING}</AppText>
    </TouchableOpacity>
  );
};

export default React.memo(LinkButton);
