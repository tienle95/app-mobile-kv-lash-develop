import AppText from 'components/AppText';
import Header from 'components/Header';
import StyledTouchable from 'components/StyledTouchable';
import { colors } from 'constants/colors';
import { scale } from 'constants/scale';
import React, { useCallback } from 'react';
import { FlatList, RefreshControl, SafeAreaView, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from './Branch.style';
import EmptyContent from 'components/EmptyContent';
import { imgDefaultBranch } from 'assets/icons';
import NavigationServices from '../../utils/navigationServices';

const BranchView = ({ data, callback, loading, onRefresh }) => {
  const renderItem = useCallback(
    ({ item }) => {
      return (
        <StyledTouchable
          style={styles.itemContainer}
          onPress={() => {
            callback?.(item);
            NavigationServices.goBack();
          }}>
          <FastImage
            source={item?.image ? { uri: item?.image } : imgDefaultBranch}
            style={styles.itemImage}
          />
          <View style={styles.infoWrapper}>
            <AppText style={styles.itemBranchName}>{item?.name}</AppText>
            <AppText style={styles.itemAddress}>{`${item?.address}`}</AppText>
          </View>
        </StyledTouchable>
      );
    },
    [callback]
  );

  const ListEmptyComponent = () => <EmptyContent />;

  return (
    <View style={styles.containerStyle}>
      <SafeAreaView>
        <Header
          title="Branch"
          hasBottomLine
          containerStyle={{ backgroundColor: colors.app_primary }}
          titleColor={colors.white}
          leftIconStyle={{ tintColor: colors.white }}
        />
      </SafeAreaView>

      <FlatList
        data={data}
        contentContainerStyle={styles.contentStyle}
        renderItem={renderItem}
        ItemSeparatorComponent={<View style={styles.lineSeparator} />}
        ListEmptyComponent={ListEmptyComponent}
        refreshControl={<RefreshControl refreshing={false} onRefresh={onRefresh} />}
      />
    </View>
  );
};

export default React.memo(BranchView);
