import { FONT_FAMILY } from 'constants/appFont';
import { colors } from 'constants/colors';
import { scale } from 'constants/scale';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    backgroundColor: colors.white
  },
  itemContainer: {
    paddingVertical: scale(8),
    paddingHorizontal: scale(16),
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: scale(1),
    borderBottomColor: colors.porcelain
  },
  itemImage: {
    width: scale(42),
    height: scale(42),
    marginRight: scale(12),
    alignSelf: 'center',
    borderRadius: scale(4),
    borderColor: colors.porcelain,
    borderWidth: 1
  },
  infoWrapper: {
    flex: 1
  },
  itemBranchName: {
    color: colors.tuna,
    fontFamily: FONT_FAMILY.SEMI_BOLD,
    fontSize: scale(15),
    lineHeight: scale(22),
    marginBottom: scale(2)
  },
  itemAddress: {
    color: colors.osloGray,
    fontFamily: FONT_FAMILY.REGULAR,
    fontSize: scale(13),
    lineHeight: scale(20),
    marginBottom: scale(2)
  },
  itemDistance: {
    color: colors.app_red,
    fontFamily: FONT_FAMILY.REGULAR,
    fontSize: scale(12),
    lineHeight: scale(20),
    marginBottom: scale(2)
  }
});

export default styles;
