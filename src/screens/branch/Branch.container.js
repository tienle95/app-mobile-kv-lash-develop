import { useRoute } from '@react-navigation/native';
import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getBranchClear, getBranchHandler } from 'redux-app/app/actions';
import { GET_BRANCH } from 'redux-app/app/constants';
import { getBranchSelector } from 'redux-app/app/selectors';
import { getLoadingSelector } from 'redux-app/loading/selector';
import BranchView from './Branch.view';

const BranchContainer = () => {
  const dispatch = useDispatch();
  const route = useRoute();

  const branchData = useSelector(getBranchSelector);
  const loading = useSelector(state => getLoadingSelector(state, [GET_BRANCH.HANDLER]));

  useEffect(() => {
    dispatch(getBranchHandler());
  }, []);

  useEffect(() => {
    return () => {
      dispatch(getBranchClear());
    };
  }, []);

  const onRefresh = useCallback(() => {
    dispatch(getBranchHandler());
  }, []);

  return (
    <BranchView
      data={branchData}
      callback={route.params?.callback}
      loading={loading}
      onRefresh={onRefresh}
    />
  );
};

export default BranchContainer;
