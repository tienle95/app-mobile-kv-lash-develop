import {
  SafeAreaView,
  View,
  FlatList,
  RefreshControl,
  TouchableOpacity,
  StatusBar,
  Platform
} from 'react-native';
import React, { useCallback } from 'react';
import styles from './Home.style';
import Header from 'components/Header';
import AppText from 'components/AppText';
import ActionButton from './components/ActionButton';
import { ic_bell, ic_bookingWhite } from 'assets/icons';
import FastImage from 'react-native-fast-image';
import { img_logoKvlash } from 'assets/images';
import CarouselView from './components/Carousel';
import Loading from 'components/Loading';
import { EMPTY_STRING } from 'constants/strings';
import NavigationServices from '../../utils/navigationServices';
import SCREENS from '../../constants/screens';
import { Gesture, GestureDetector } from 'react-native-gesture-handler';
import Animated, {
  useAnimatedStyle,
  useDerivedValue,
  useSharedValue,
  withSpring
} from 'react-native-reanimated';
import { DEVICE } from 'constants/scale';
import PopupPromotion from './components/PopupPromotion';
import { openUrl } from 'utils/openUrl';

const useFollowAnimatedPosition = ({ x, y }) => {
  const followX = useDerivedValue(() => {
    return withSpring(x.value);
  });

  const followY = useDerivedValue(() => {
    return withSpring(y.value);
  });

  const rStyle = useAnimatedStyle(() => {
    return {
      transform: [{ translateX: followX.value }, { translateY: followY.value }]
    };
  });

  return { followX, followY, rStyle };
};

const HomeScreen = props => {
  const {
    data,
    introduceData,
    popupPromotion,
    loading,
    buttonAction,
    goToWebView,
    onRefresh,
    onPressBooking,
    isVisiblePopupPromotion,
    setisVisiblePopupPromotion
  } = props;

  const translateX = useSharedValue(DEVICE.WIDTH - 66);
  const translateY = useSharedValue(-100);

  const context = useSharedValue({ x: 0, y: 0 });

  const gesture = Gesture.Pan()
    .onStart(() => {
      context.value = { x: translateX.value, y: translateY.value };
    })
    .onUpdate(event => {
      translateX.value = event.translationX + context.value.x;
      translateY.value = event.translationY + context.value.y;
    })
    .onEnd(() => {
      if (translateX.value > DEVICE.WIDTH / 2) {
        translateX.value = DEVICE.WIDTH - 66;
      } else {
        translateX.value = 16;
      }
      if (translateY.value > -30) {
        translateY.value = -30;
      } else if (translateY.value < -(DEVICE.HEIGHT - 130)) {
        translateY.value = -(DEVICE.HEIGHT - 140);
      }
    });

  // const followX = useDerivedValue(() => {
  //   return withSpring(translateX.value);
  // });

  // const followY = useDerivedValue(() => {
  //   return withSpring(translateY.value);
  // });

  // const rStyle = useAnimatedStyle(() => {
  //   return {
  //     transform: [{ translateX: followX.value }, { translateY: followY.value }]
  //   };
  // });

  const { followX, followY, rStyle } = useFollowAnimatedPosition({ x: translateX, y: translateY });

  const renderItem = useCallback(({ item, index }) => {
    return (
      <ActionButton
        title={item?.title}
        icon={item?.icon}
        description={item?.description}
        containerStyle={[
          styles.buttonContainerStyle,
          index % 2 === 0 && styles.marginRight,
          { backgroundColor: item?.bgColor }
        ]}
        onPress={() => buttonAction(item)}
      />
    );
  }, []);

  const renderHeader = useCallback(() => {
    return (
      <View>
        <View style={styles.welcomeContainer}>
          <AppText numberOfLines={1} style={styles.welcome} translate>
            {'PAY CASH GET DISCOUNT!'}
          </AppText>
          {/* <FastImage style={styles.iconHello} source={ic_hello} resizeMode={'contain'} /> */}
        </View>
        <CarouselView data={data?.banner} onPress={goToWebView} />
      </View>
    );
  }, [data]);

  const renderItemAboutUs = useCallback((item, index) => {
    return (
      <View key={`${item?.id}_${index}`} style={styles.itemFooterContainer}>
        <FastImage style={styles.itemFooterImage} source={{ uri: item?.image }} />
        <View style={styles.footerContentContainer}>
          <AppText style={styles.footerItemTitle}>{item?.title || EMPTY_STRING}</AppText>
          <AppText style={styles.footerItemDes}>{item?.description || EMPTY_STRING}</AppText>
        </View>
      </View>
    );
  }, []);

  const renderFooter = useCallback(() => {
    return (
      <View style={styles.aboutUsContainer}>
        <AppText numberOfLines={1} style={styles.titleAboutUs}>
          {'Book online at:'} <AppText onPress={()=> openUrl('https://www.kvlash.com/')}
          style={[styles.titleAboutUs, {textDecorationLine: 'underline'}]}
          >kvlash.com</AppText>
        </AppText>
        {introduceData?.map((item, index) => renderItemAboutUs(item, index))}
      </View>
    );
  }, [introduceData, renderItemAboutUs]);

  return (
    <View style={styles.container}>
      <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'} />
      {loading && <Loading />}
      <PopupPromotion
        isVisible={isVisiblePopupPromotion}
        setIsVisible={setisVisiblePopupPromotion}
        data={popupPromotion?.[0]}
      />
      <SafeAreaView>
        <Header
          isHome
          logo={img_logoKvlash}
          rightIcon={ic_bell}
          onPressRightIcon={() => NavigationServices.navigate(SCREENS.NOTIFICATION)}
        />
      </SafeAreaView>

      {/* <GestureDetector gesture={gesture}>
        <Animated.View style={[styles.iconContainer, rStyle]}>
          <TouchableOpacity
            style={styles.iconBookingTouchContainer}
            activeOpacity={1}
            onPress={onPressBooking}>
            <FastImage style={styles.icon} source={ic_bookingWhite} />
          </TouchableOpacity>
        </Animated.View>
      </GestureDetector> */}

      <FlatList
        data={data?.button}
        keyExtractor={(item, index) => `${item?.id}_${index}`}
        numColumns={2}
        renderItem={renderItem}
        ItemSeparatorComponent={<View style={styles.itemSeparatorComponent} />}
        ListHeaderComponent={renderHeader}
        ListHeaderComponentStyle={styles.listHeaderComponentStyle}
        ListFooterComponent={renderFooter}
        ListFooterComponentStyle={styles.listFooterComponentStyle}
        contentContainerStyle={styles.flatlistStyle}
        refreshControl={<RefreshControl refreshing={false} onRefresh={onRefresh} />}
      />
    </View>
  );
};

export default HomeScreen;
