import React, { useCallback, useEffect, useState } from 'react';
import HomeView from './Home.view';
import NavigationServices from '../../utils/navigationServices';
import SCREENS from '../../constants/screens';
import { useDispatch, useSelector } from 'react-redux';
import {
  getAppHomeSelector,
  getIntroduceSelector,
  getPopupPromotionSelector
} from 'redux-app/app/selectors';
import {
  getAppHomeHandler,
  getHomeClear,
  getIntroduceHandler,
  getPopupPromotionHandler
} from 'redux-app/app/actions';
import { ACTION_CODE } from 'constants/string';
import { getLoadingSelector } from 'redux-app/loading/selector';
import { GET_HOME, GET_INTRODUCE, GET_POPUP_PROMOTION } from 'redux-app/app/constants';
import { Linking } from 'react-native';
import { openUrl } from 'utils/openUrl';
import { useIsFocused } from '@react-navigation/native';

const HomeContainer = () => {
  const dispatch = useDispatch();
  const isFocused = useIsFocused();

  const [isVisiblePopupPromotion, setisVisiblePopupPromotion] = useState(false);

  const loading = useSelector(state =>
    getLoadingSelector(state, [
      GET_HOME.HANDLER,
      GET_INTRODUCE.HANDLER,
      GET_POPUP_PROMOTION.HANDLER
    ])
  );

  const homeData = useSelector(getAppHomeSelector);
  const introduceData = useSelector(getIntroduceSelector);
  // const popupPromotion = useSelector(getPopupPromotionSelector);
  const [popupPromotion, setPopupPromotion] = useState([]);

  useEffect(() => {
    if (isFocused) {
      dispatch(
        getPopupPromotionHandler(
          {},
          res => {
            if (res?.length > 0) {
              setPopupPromotion(res);
              setisVisiblePopupPromotion(true);
            }
          },
          () => {}
        )
      );
    }
  }, [isFocused]);

  useEffect(() => {
    dispatch(getAppHomeHandler());
  }, []);

  useEffect(() => {
    dispatch(getIntroduceHandler());
  }, []);

  useEffect(() => {
    return () => {
      dispatch(getHomeClear());
    };
  }, []);

  // useEffect(() => {
  //   if (isFocused && popupPromotion?.length > 0) {
  //     setisVisiblePopupPromotion(true);
  //   }
  // }, [isFocused]);

  const buttonAction = useCallback(item => {
    switch (item?.code) {
      case ACTION_CODE.BOOKING:
        NavigationServices.navigate(SCREENS.BOOKING_ONLINE);
        break;
      case ACTION_CODE.APPOINTMENT:
        NavigationServices.navigate(SCREENS.MY_APPOINTMENT);
        break;
      case ACTION_CODE.SHOP:
        // NavigationServices.navigate(SCREENS.WEB_VIEW, { link: item?.link });
        openUrl(item?.link);
        break;
      case ACTION_CODE.GIFT_CARD:
        // NavigationServices.navigate(SCREENS.WEB_VIEW, { link: item?.link });
        openUrl(item?.link);
        break;
      case ACTION_CODE.CUSTOMER_SERVICE:
        Linking.openURL(`tel:${item?.link}`);
        break;
      case ACTION_CODE.CONTACT_US:
        NavigationServices.navigate(SCREENS.CONTACT_US);
        break;
    }
  }, []);

  const goToWebView = useCallback(item => {
    // NavigationServices.navigate(SCREENS.WEB_VIEW, { link: item?.link });
    openUrl(item?.link);
  }, []);

  const onPressBooking = useCallback(() => {
    NavigationServices.navigate(SCREENS.BOOKING);
  }, []);

  const onRefresh = useCallback(() => {
    dispatch(getAppHomeHandler());
    dispatch(getIntroduceHandler());
    dispatch(
      getPopupPromotionHandler(
        {},
        res => {
          if (res?.length > 0) {
            setPopupPromotion(res);
            setisVisiblePopupPromotion(true);
          }
        },
        () => {}
      )
    );
  }, []);

  return (
    <HomeView
      data={homeData}
      introduceData={introduceData}
      popupPromotion={popupPromotion}
      loading={loading}
      goToWebView={goToWebView}
      buttonAction={buttonAction}
      onRefresh={onRefresh}
      onPressBooking={onPressBooking}
      isVisiblePopupPromotion={isVisiblePopupPromotion}
      setisVisiblePopupPromotion={setisVisiblePopupPromotion}
    />
  );
};

export default HomeContainer;
