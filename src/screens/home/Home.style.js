import { FONT_FAMILY } from 'constants/appFont';
import { colors } from 'constants/colors';
import { DEVICE, scale } from 'constants/scale';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.light
  },
  text: {
    fontSize: scale(20),
    alignSelf: 'center',
    color: colors.light
    // fontFamily: FONT_FAMILY.REGULAR
  },
  groupsButtonContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: scale(16)
  },
  buttonContainerStyle: { marginBottom: scale(8) },
  marginRight: { marginRight: scale(5) },
  button: {
    margin: 10,
    borderRadius: 10,
    padding: 10,
    backgroundColor: 'blue'
  },
  titleHeader: {
    fontSize: scale(18),
    color: colors.dark
    // fontFamily: FONT_FAMILY.REGULAR
  },
  scroll: {
    paddingBottom: scale(16)
  },
  flatlistStyle: { paddingHorizontal: scale(16) },
  listHeaderComponentStyle: { marginHorizontal: scale(-16), marginBottom: scale(16) },
  listFooterComponentStyle: { marginHorizontal: scale(-16), marginBottom: scale(16) },
  itemBannerContainer: {},
  itemBannerStyle: { width: DEVICE.WIDTH },
  title: {
    flex: 1,
    fontSize: scale(16),
    color: colors.dark,
    fontFamily: FONT_FAMILY.SEMI_BOLD,
    marginVertical: scale(16),
    lineHeight: scale(24)
  },
  welcomeContainer: {
    flexDirection: 'row',
    paddingHorizontal: scale(16),
    marginVertical: scale(8),
    alignItems: 'center'
  },
  welcome: {
    fontSize: scale(24),
    color: colors.dark,
    fontFamily: FONT_FAMILY.BOLD,
    // lineHeight: scale(24),
    marginRight: scale(8)
  },
  iconHello: { width: scale(24), height: scale(32), alignSelf: 'center' },
  aboutUsContainer: {},
  titleAboutUs: {
    flex: 1,
    fontSize: scale(16),
    color: colors.dark,
    fontFamily: FONT_FAMILY.SEMI_BOLD,
    marginVertical: scale(8),
    paddingHorizontal: scale(16),
    lineHeight: scale(24)
  },
  itemFooterImage: {
    width: DEVICE.WIDTH,
    height: scale(207),
    resizeMode: 'cover'
  },
  footerItemTitle: {
    fontSize: scale(14),
    color: colors.dark,
    fontFamily: FONT_FAMILY.SEMI_BOLD
  },
  footerItemDes: { fontSize: scale(12), color: colors.tuna, fontFamily: FONT_FAMILY.REGULAR },
  itemFooterContainer: { marginBottom: scale(16) },
  footerContentContainer: {
    marginTop: scale(16),
    paddingHorizontal: scale(16)
  },
  iconContainer: {
    zIndex: 999,
    position: 'absolute',
    bottom: 0,
    left: 0
    // backgroundColor: colors.radicalRed,
    // width: scale(52),
    // height: scale(52),
    // borderRadius: scale(25)
    // alignItems: 'center',
    // justifyContent: 'center'
  },
  iconBookingTouchContainer: {
    width: scale(52),
    height: scale(52),
    borderRadius: scale(25),
    backgroundColor: colors.radicalRed,
    alignItems: 'center',
    justifyContent: 'center'
  },
  icon: { width: scale(24), height: scale(24) }
});
export default styles;
