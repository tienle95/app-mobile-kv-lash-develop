// import { FONT_SIZE } from 'constants/appFonts';
import { colors } from 'constants/colors';
import { DEVICE, scale } from 'constants/scale';
import { StyleSheet } from 'react-native';

const SPACING = 5;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  itemContent: {
    // marginHorizontal: SPACING * 3,
    alignItems: 'center',
    backgroundColor: 'white'
  },
  itemImage: {
    width: DEVICE.WIDTH,
    height: scale(207),
    resizeMode: 'cover'
  },
  flatListContent: { alignItems: 'center' },
  itemBannerStyle: { width: DEVICE.WIDTH, height: scale(200) },
  dotsContainer: {
    flex: 1,
    position: 'absolute',
    height: scale(10),
    alignSelf: 'center',
    bottom: scale(5),
    flexDirection: 'row'
  },
  dot: {
    backgroundColor: colors.alto,
    width: scale(6),
    height: scale(6),
    borderRadius: scale(10),
    marginRight: scale(4)
  },
  activeDot: { backgroundColor: colors.primary_color }
});

export default styles;
