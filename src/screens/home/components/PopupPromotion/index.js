import { TouchableHighlight, View } from 'react-native';
import Modal from 'react-native-modal';
import React, { useCallback } from 'react';
import FastImage from 'react-native-fast-image';
import styles from './styles';
import { ic_close } from 'assets/icons';
import { openUrl } from 'utils/openUrl';

const PopupPromotion = ({ isVisible, setIsVisible, data }) => {
  const closePopup = useCallback(() => {
    setIsVisible(false);
  }, []);

  const onPressPromotion = useCallback(() => {
    if (data?.link) {
      openUrl(data?.link);
    }
  }, [data?.link]);

  return (
    <Modal
      isVisible={isVisible}
      animationIn={'fadeInUp'}
      animationOut={'fadeOutDown'}
      onBackdropPress={closePopup}>
      <View style={{ alignSelf: 'center' }}>
        <TouchableHighlight underlayColor="#fff" onPress={onPressPromotion}>
          <FastImage
            style={styles.image}
            source={{
              uri: data?.image
            }}
          />
        </TouchableHighlight>
        <TouchableHighlight
          underlayColor="transparent"
          onPress={closePopup}
          style={styles.iconCloseContainer}>
          <FastImage style={styles.iconClose} source={ic_close} tintColor={'#ADB1B9'} />
        </TouchableHighlight>
      </View>
    </Modal>
  );
};

export default React.memo(PopupPromotion);
