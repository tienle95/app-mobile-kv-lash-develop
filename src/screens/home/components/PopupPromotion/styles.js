import { scale } from 'constants/scale';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image: {
    width: scale(327),
    height: scale(327),
    borderRadius: scale(8)
  },
  iconCloseContainer: { position: 'absolute', top: scale(8), right: scale(8) },
  iconClose: {
    width: scale(20),
    height: scale(20)
  }
});

export default styles;
