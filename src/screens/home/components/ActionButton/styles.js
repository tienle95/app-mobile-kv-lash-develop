import { FONT_FAMILY } from 'constants/appFont';
import { colors } from 'constants/colors';
import { scale } from 'constants/scale';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: scale(4),
    padding: scale(8),
    backgroundColor: colors.primary_color,
    flexDirection: 'row',
    alignItems: 'center'
  },
  iconContainer: {
    backgroundColor: colors.light,
    borderRadius: scale(4)
  },
  icon: { width: scale(36), height: scale(36) },
  contentContainer: { flex: 1, marginLeft: scale(5), alignItems: 'center' },
  title: {
    fontFamily: FONT_FAMILY.SEMI_BOLD,
    fontSize: scale(10),
    color: colors.light
  },
  description: {
    fontFamily: FONT_FAMILY.REGULAR,
    fontSize: scale(8),
    color: colors.light
  },
  disable: {
    opacity: 0.5
  }
});

export default styles;
