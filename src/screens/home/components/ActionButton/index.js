import { TouchableOpacity, View } from 'react-native';
import React from 'react';
import styles from './styles';
import AppText from 'components/AppText';
import { EMPTY_STRING } from 'constants/strings';
import FastImage from 'react-native-fast-image';

const ActionButton = ({
  containerStyle,
  icon,
  title,
  titleStyle,
  description,
  descriptionStyle,
  onPress,
  ...props
}) => {
  return (
    <TouchableOpacity style={[styles.container, containerStyle]} onPress={onPress} {...props}>
      <View style={styles.iconContainer}>
        <FastImage style={styles.icon} source={{ uri: icon }} />
      </View>
      <View style={styles.contentContainer}>
        <AppText style={[styles.title, titleStyle]}>{title || EMPTY_STRING}</AppText>
        {description && (
          <AppText style={[styles.description, descriptionStyle]}>
            {description || EMPTY_STRING}
          </AppText>
        )}
      </View>
    </TouchableOpacity>
  );
};

export default React.memo(ActionButton);
