import { FONT_FAMILY } from 'constants/appFont';
import { colors } from 'constants/colors';
import { DEVICE, scale } from 'constants/scale';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
  },
  scrollViewContainer: { flex: 1, paddingHorizontal: scale(16) },
  contentStyle: {
    paddingVertical: scale(16)
  },
  title: {
    fontFamily: FONT_FAMILY.SEMI_BOLD,
    fontSize: scale(16),
    color: colors.tuna
  },
  icRead: { width: scale(16), height: scale(16) },
  seenContainer: { flexDirection: 'row', alignItems: 'center', marginTop: scale(8) },
  itemDate: {
    marginLeft: scale(8)
  },
  content: {
    fontFamily: FONT_FAMILY.REGULAR,
    fontSize: scale(14),
    color: colors.tuna,
    marginVertical: scale(8),
    lineHeight: scale(24)
  },
  image: { width: DEVICE.WIDTH - scale(32), height: scale(200) },
  buttonContainer: {
    paddingHorizontal: scale(16),
    paddingTop: scale(8),
    paddingBottom: scale(20),
    backgroundColor: colors.white
  },
  btnContainer: {
    width: DEVICE.WIDTH - scale(32),
    height: scale(48),
    backgroundColor: colors.primary_color,
    borderRadius: scale(8)
  }
});
export default styles;
