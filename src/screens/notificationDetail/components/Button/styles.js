// import { FONT_SIZE } from 'constants/appFonts';
import { FONT_FAMILY } from 'constants/appFont';
import { colors } from 'constants/colors';
import { scale } from 'constants/scale';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    borderRadius: scale(8),
    paddingVertical: scale(12),
    paddingHorizontal: scale(8),
    backgroundColor: '#00ADEF'
  },
  title: {
    fontFamily: FONT_FAMILY.SEMI_BOLD,
    fontSize: scale(14),
    color: colors.white,
    alignSelf: 'center'
  }
});

export default styles;
