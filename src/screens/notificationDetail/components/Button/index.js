import { TouchableOpacity } from 'react-native';
import React from 'react';
import styles from './styles';
import AppText from 'components/AppText';
import { EMPTY_STRING } from 'constants/strings';

const Button = ({ containerStyle, title, titleStyle, onPress, ...props }) => {
  return (
    <TouchableOpacity style={[styles.container, containerStyle]} onPress={onPress} {...props}>
      <AppText style={[styles.title, titleStyle]}>{title || EMPTY_STRING}</AppText>
    </TouchableOpacity>
  );
};

export default React.memo(Button);
