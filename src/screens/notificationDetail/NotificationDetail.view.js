import { SafeAreaView, ScrollView, View } from 'react-native';
import React from 'react';
import { ic_read } from 'assets/icons';
import { img_detail1 } from 'assets/images';
import AppText from 'components/AppText';
import Header from 'components/Header';
import Loading from 'components/Loading';
import { EMPTY_STRING } from 'constants/strings';
import FastImage from 'react-native-fast-image';
import { colors } from '../../constants/colors';
import Button from './components/Button';
import styles from './NotificationDetail.style';
import moment from 'moment';
import { scale } from '../../constants/scale';
import AppButton from '../../components/AppButton';

const DetailInfoScreen = props => {
  const { data, loading, handleLinking } = props;

  return (
    <View style={styles.container}>
      {loading && <Loading />}
      <SafeAreaView>
        <Header
          back
          title={'DETAIL'}
          hasBottomLine
          containerStyle={{ backgroundColor: '#00ADEF' }}
        />
      </SafeAreaView>

      <ScrollView style={styles.scrollViewContainer} contentContainerStyle={styles.contentStyle}>
        <AppText size="L" weight="semi-bold">
          {data?.title}
        </AppText>
        <View style={styles.seenContainer}>
          <FastImage style={styles.icRead} source={ic_read} />
          <AppText
            color={'#2F3542'}
            size="XS"
            line={scale(24)}
            numberOfLines={1}
            style={styles.itemDate}>
            {moment(data?.send_at).format('HH:mm - DD/MM/YYYY') || EMPTY_STRING}
          </AppText>
        </View>
        <AppText style={styles.content}>{data?.content}</AppText>
      </ScrollView>
      <View style={styles.buttonContainer}>
        <AppButton style={styles.btnContainer} onPress={() => handleLinking()}>
          <AppText weight="semi-bold" color={colors.white}>
            {'View detail'}
          </AppText>
        </AppButton>
      </View>
    </View>
  );
};

export default DetailInfoScreen;
