import React, { useCallback, useEffect } from 'react';
import DetailInfoView from './NotificationDetail.view';
import { useDispatch } from 'react-redux';
import { getAppointmentClear, getAppointmentHandler } from 'redux-app/app/actions';
import { openUrl } from 'utils/openUrl';
import { useRoute } from '@react-navigation/native';

const NotificationDetailScreen = () => {
  const dispatch = useDispatch();
  const route = useRoute();

  //   const loading = useSelector(state => getLoadingSelector(state, [GET_APPOINTMENT.HANDLER]));
  const loading = false;

  const dataDetailInfo = route?.params?.item;
  // const appointmentData = useSelector(getAppointmentSelector);

  useEffect(() => {
    dispatch(getAppointmentHandler());
  }, [dispatch]);

  useEffect(() => {
    return () => {
      dispatch(getAppointmentClear());
    };
  }, [dispatch]);

  const handleLinking = useCallback(() => {
    openUrl(dataDetailInfo?.link);
  }, []);

  return <DetailInfoView data={dataDetailInfo} loading={loading} handleLinking={handleLinking} />;
};

export default NotificationDetailScreen;
