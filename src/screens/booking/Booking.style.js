import { StyleSheet } from 'react-native';
import { colors } from '../../constants/colors';
import { DEVICE, scale, STATUS_BAR_HEIGHT } from '../../constants/scale';

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: colors.white
  },
  formContent: {
    flexGrow: 1,
    paddingTop: scale(50),
    paddingBottom: scale(100)
  },
  headerImg: {
    height: scale(200),
    width: DEVICE.WIDTH
  },
  headerContent: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: STATUS_BAR_HEIGHT,
    justifyContent: 'space-between',
    paddingHorizontal: scale(16)
  },
  iconBack: {
    width: scale(24),
    height: scale(24)
  },
  body: {
    width: DEVICE.WIDTH,
    top: scale(-100)
  },
  formContainer: {
    marginHorizontal: scale(16),
    paddingHorizontal: scale(16),
    paddingBottom: scale(16),
    width: DEVICE.WIDTH - scale(32),
    backgroundColor: colors.white,

    borderRadius: scale(8),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.3,
    shadowRadius: 5,
    elevation: 2
  },
  sendBtn: isDisableBtn => ({
    height: scale(48),
    backgroundColor: isDisableBtn ? colors.disable_color : colors.primary_color,
    marginBottom: scale(32),
    marginHorizontal: scale(16),
    borderRadius: scale(8),
    marginTop: scale(16)
  })
});

export default styles;
