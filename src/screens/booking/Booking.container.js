import moment from 'moment';
import React, { useCallback, useMemo, useRef, useState } from 'react';
import { showModal } from '../../components/Modal/ModalNotification';
import SCREENS from '../../constants/screens';
import { submitBookingApi } from '../../services/api/candidate';
import NavigationServices from '../../utils/navigationServices';
import BookingView from './Booking.view';

const BookingScreen = () => {
  const [branch, setBranch] = useState(null);
  const formRef = useRef();
  const initialValues = {
    name: '',
    phoneNumber: '',
    bookingDate: '',
    appointmentTime: '',
    branch: '',
    note: ''
  };

  const isDisableBtn =
    !formRef.current?.values['name'] ||
    !formRef.current?.values['phoneNumber'] ||
    !formRef.current?.values['bookingDate'] ||
    !formRef.current?.values['appointmentTime'] ||
    !branch?.id;

  const handleSubmit = useCallback(
    async e => {
      try {
        const res = await submitBookingApi({
          params: {
            name: e.name,
            phone_number: e.phoneNumber,
            booking_date: new Date(
              moment(e.bookingDate).format('YYYY/MM/DD') +
                ' ' +
                moment(e.appointmentTime).format('HH:mm')
            ),
            branch_id: branch.id,
            note: e.note
          }
        });
        console.log('res', res);
        if (res?.status === 200) {
          showModal({
            type: 'success',
            title: 'Successful',
            content: 'You have successfully submitted your booking request',
            onConfirm: () => NavigationServices.navigate(SCREENS.HOME),
            confirmText: 'Completed'
          });
        }
      } catch (error) {
        console.log('error', error);
      }
    },
    [branch]
  );

  const onSelectBranch = value => {
    setBranch(value);
    formRef.current?.setFieldValue('branch', value?.name);
  };

  const genValues = useMemo(
    () => [
      {
        type: 'input',
        name: 'name',
        placeholder: 'Enter your first and last name',
        title: 'Name',
        isOutFocused: true,
        isTrim: true,
        isRequired: true
      },
      {
        type: 'input',
        typeInput: 'phone',
        name: 'phoneNumber',
        title: 'Phone Number',
        placeholder: 'Enter your phone number',
        isOutFocused: true,
        isTrim: true,
        isRequired: true
      },
      {
        type: 'date',
        name: 'bookingDate',
        title: 'Booking Date',
        placeholder: 'Choose an appointment date',
        isOutFocused: true,
        isTrim: true,
        isRequired: true,
        minimumDate: new Date()
      },
      {
        type: 'date',
        mode: 'time',
        name: 'appointmentTime',
        title: 'Appointment time',
        placeholder: 'Choose your appointment time',
        isOutFocused: true,
        isTrim: true,
        isRequired: true
      },
      {
        type: 'select',
        name: 'branch',
        title: 'Branch',
        placeholder: 'Choose branch',
        isRequired: true,
        screen: SCREENS.BRANCH,
        additionalParams: {
          callback: onSelectBranch
        }
      },
      {
        type: 'textarea',
        name: 'note',
        title: 'Note',
        placeholder: 'Enter a note for better service',
        isOutFocused: true,
        isTrim: true
      }
    ],
    []
  );

  return (
    <BookingView
      genValues={genValues}
      formRef={formRef}
      initialValues={initialValues}
      handleSubmit={handleSubmit}
      isDisableBtn={isDisableBtn}
    />
  );
};

export default BookingScreen;
