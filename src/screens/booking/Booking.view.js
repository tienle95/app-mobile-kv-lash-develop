import React from 'react';
import { ImageBackground, ScrollView, StatusBar, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { ic_back } from '../../assets/icons';
import { img_header_booking } from '../../assets/images';
import AppButton from '../../components/AppButton';
import AppText from '../../components/AppText';
import Form from '../../components/FormInput/Form';
import StyledTouchable from '../../components/StyledTouchable';
import { colors } from '../../constants/colors';
import NavigationServices from '../../utils/navigationServices';
import styles from './Booking.style';

const BookingView = ({ formRef, initialValues, genValues, handleSubmit, isDisableBtn }) => {
  return (
    <View style={styles.container}>
      <ImageBackground source={img_header_booking} style={styles.headerImg} resizeMode="cover">
        <StatusBar barStyle="light-content" />
        <View style={styles.headerContent}>
          <StyledTouchable style={styles.iconBack} onPress={() => NavigationServices.goBack()}>
            <FastImage source={ic_back} style={styles.iconBack} tintColor={colors.white} />
          </StyledTouchable>
          <AppText size="X3L" weight="semi-bold" color={colors.white}>
            {'BOOKING'}
          </AppText>
          <View style={styles.iconBack} />
        </View>
      </ImageBackground>
      <KeyboardAwareScrollView
        scrollIndicatorInsets={{ right: 1 }}
        enableOnAndroid={true}
        extraHeight={250}
        style={styles.body}
        contentContainerStyle={styles.formContent}>
        <View style={styles.formContainer}>
          <Form
            ref={formRef}
            initialValues={initialValues}
            genValues={genValues}
            onSubmit={handleSubmit}
            customInputStyle={styles.input}
            enableReinitialize
            validateOnChange={false}
            validateOnBlur={false}
            view={View}
          />
        </View>
        <AppButton
          disabled={isDisableBtn}
          style={styles.sendBtn(isDisableBtn)}
          onPress={() => formRef.current?.handleSubmit()}>
          <AppText color={colors.white}>{'Send request'}</AppText>
        </AppButton>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default React.memo(BookingView);
