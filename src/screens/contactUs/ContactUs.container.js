import React, { useCallback, useEffect } from 'react';
import ContactUsView from './ContactUs.view';
import { useDispatch, useSelector } from 'react-redux';
import { getContactSelector } from 'redux-app/app/selectors';
import { getContactClear, getContactHandler } from 'redux-app/app/actions';
import { getLoadingSelector } from 'redux-app/loading/selector';
import { GET_CONTACT } from 'redux-app/app/constants';
import { CONTACT_TYPE } from 'constants/string';
import { Linking } from 'react-native';
import { openUrl } from 'utils/openUrl';
import { LinkingContext } from '@react-navigation/native';

const ContactUsContainer = () => {
  const dispatch = useDispatch();

  const contactUsData = useSelector(getContactSelector);
  const loading = useSelector(state => getLoadingSelector(state, [GET_CONTACT.HANDLER]));

  useEffect(() => {
    dispatch(getContactHandler());
  }, []);

  useEffect(() => {
    return () => {
      dispatch(getContactClear());
    };
  }, []);

  const linking = useCallback(item => {
    switch (item?.type) {
      case CONTACT_TYPE.PHONE:
        Linking.openURL(`tel:${item?.value}`);
        break;

      case CONTACT_TYPE.EMAIL:
        Linking.openURL(`mailto:${item?.value}`);
        break;
    }
  }, []);

  return <ContactUsView data={contactUsData} loading={loading} linking={linking} />;
};

export default ContactUsContainer;
