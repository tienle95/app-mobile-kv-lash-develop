// import { FONT_SIZE } from 'constants/appFonts';
import { FONT_FAMILY } from 'constants/appFont';
import { colors } from 'constants/colors';
import { scale } from 'constants/scale';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: scale(4),
    padding: scale(8),
    backgroundColor: colors.primary_color
  },
  iconContainer: {
    backgroundColor: colors.light,
    borderRadius: scale(4),
    padding: scale(7)
  },
  icon: { width: scale(22), height: scale(22) },
  title: {
    fontFamily: FONT_FAMILY.SEMI_BOLD,
    fontSize: scale(14),
    color: colors.light,
    marginBottom: scale(8)
  },
  value: {
    fontFamily: FONT_FAMILY.REGULAR,
    fontSize: scale(12),
    color: colors.light
  },
  disable: {
    opacity: 0.5
  }
});

export default styles;
