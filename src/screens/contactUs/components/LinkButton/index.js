import { TouchableOpacity } from 'react-native';
import React from 'react';
import styles from './styles';
import AppText from 'components/AppText';
import { EMPTY_STRING } from 'constants/strings';

const LinkButton = ({
  containerStyle,
  title,
  titleStyle,
  value,
  valueStyle,
  onPress,
  ...props
}) => {
  return (
    <TouchableOpacity style={[styles.container, containerStyle]} onPress={onPress} {...props}>
      <AppText style={[styles.title, titleStyle]}>{title || EMPTY_STRING}</AppText>
      <AppText style={[styles.value, valueStyle]}>{value || EMPTY_STRING}</AppText>
    </TouchableOpacity>
  );
};

export default React.memo(LinkButton);
