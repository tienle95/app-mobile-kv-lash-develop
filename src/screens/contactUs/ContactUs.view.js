import { FlatList, SafeAreaView, View } from 'react-native';
import React, { useCallback } from 'react';
import styles from './ContactUs.style';
import Header from 'components/Header';
import LinkButton from './components/LinkButton';
import Loading from 'components/Loading';

const ContactUsScreen = props => {
  const { data, loading, linking } = props;

  const renderItem = useCallback(({ item }) => {
    return <LinkButton title={item?.title} value={item?.value} onPress={() => linking(item)} />;
  }, []);

  return (
    <View style={styles.container}>
      {loading && <Loading />}
      <SafeAreaView>
        <Header back title={'Contact Us'} />
      </SafeAreaView>

      <FlatList
        data={data}
        keyExtractor={item => item.id}
        renderItem={renderItem}
        ItemSeparatorComponent={<View style={styles.itemSeparatorComponent} />}
        style={styles.flatlistStyle}
      />
    </View>
  );
};

export default ContactUsScreen;
