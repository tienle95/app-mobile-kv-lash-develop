import { scale } from './scale';

const DEFAULT_PREFIX_FONT_FAMILY = 'Poppins-';

export const FONT_FAMILY = {
  BOLD: `${DEFAULT_PREFIX_FONT_FAMILY}Bold`,
  LIGHT: `${DEFAULT_PREFIX_FONT_FAMILY}Light`,
  MEDIUM: `${DEFAULT_PREFIX_FONT_FAMILY}Medium`,
  REGULAR: `${DEFAULT_PREFIX_FONT_FAMILY}Regular`,
  SEMI_BOLD: `${DEFAULT_PREFIX_FONT_FAMILY}SemiBold`
};

// fontsize
export const FONT_SIZE = {
  extraLarge: scale(24),
  headerScreen: scale(20),
  large: scale(18),
  regular: scale(16),
  small: scale(14),
  verySmall: scale(12),
  mediumLarge: scale(22)
};
