export const TYPE_VIEW = {
  VIEW: 'View',
  SAFE_AREA_VIEW: 'SafeAreaView'
};

export const FIELD_EXPERIENCE = {
  COMPANY: 'company',
  ID_COMPANY: 'id_Company',
  POSITION: 'position',
  START_DATE: 'startDate',
  END_DATE: 'endDate',
  SUMMARY: 'summary'
};

export const FIELD_CONTACT_INFO = {
  PHONE_NUMBERS: 'phoneNumbers',
  EMAIL_ADDRESS: 'emailAddress',
  IMS: 'ims',
  WEBSITES: 'websites'
};

export const FIELD_ACHIVEMENT = {
  TITLE: 'title',
  DATE: 'date'
};

export const FIELD_EDUCATION = {
  INSTITUTION: 'institution',
  STUDY_TYPE: 'studyType',
  AREA: 'area',
  START_DATE: 'startDate',
  END_DATE: 'endDate'
};

export const FIELD_PERSONAL_INFO_CANDIDATE = {
  BIRTH_DAY: 'birthday',
  GENDER: 'gender',
  SOURCE: 'source',
  EXPERIENCE_YEAR: 'experienceYears',
  LABEL: 'label',
  CHANNEL: 'channel',
  NATIONALITIES: 'nationalities',
  LOCATION: 'location',
  EXPECT_WORKING_PLACE: 'expectWorkingPlace'
};

export const FIELD_SKILL_LANGUAGE = {
  SKILL: 'skills',
  LANGUAGE: 'languages'
};

export const GENDER = [
  {
    id: 1,
    isCheck: true,
    label: 'common.male',
    value: 0
  },
  {
    id: 2,
    isCheck: false,
    label: 'common.female',
    value: 1
  },
  {
    id: 3,
    isCheck: false,
    label: 'common.unknown',
    value: 2
  }
];

export const LIST_DROPDOWN = [
  {
    id: 1,
    isActive: true,
    zIndex: 30
  },
  {
    id: 2,
    isActive: false,
    zIndex: 10
  },
  {
    id: 3,
    isActive: false,
    zIndex: 10
  }
];

export const DEFAULT_NUMBER_OF_LINE = 1;

export const TAB_CV = {
  ACTIVATE_ITEM: 1,
  DEACTIVATE_ITEM: 2
};

export const TAB_TYPE_DOCUMENT = [
  { id: 1, value: 'createCandidate.pdf' },
  { id: 2, value: 'createCandidate.text' }
];
