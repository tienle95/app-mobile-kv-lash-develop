const SCREENS = {
  HOME: 'Home',
  BOOKING_ONLINE: 'BookingOnline',
  MY_APPOINTMENT: 'MyAppointment',
  WEB_VIEW: 'WebView',
  CONTACT_US: 'ContactUs',
  BRANCH: 'Branch',
  NOTIFICATION: 'Notification',
  NOTIFICATION_DETAIL: 'NotificationDetail',
  BOOKING: 'Booking'
};

export default SCREENS;
