import AppConfigs from 'configs/env';

const BASE_URL = AppConfigs.BASE_URL;

const END_POINTS = {
  GET_LIST_CANDIDATE: BASE_URL + '/candidates/list',
  GET_HOME: BASE_URL + 'home',
  GET_BOOKING: BASE_URL + 'bookingonline',
  GET_APPOINTMENT: BASE_URL + 'myappointment',
  GET_CONTACT: BASE_URL + 'contactus',
  GET_INTRODUCE: BASE_URL + 'introduce',
  GET_BRANCH: BASE_URL + 'branch',
  BOOKING: BASE_URL + 'booking',
  GET_POPUP_PROMOTION: BASE_URL + 'popup',
  NOTIFICATION: BASE_URL + 'notification'
};

export { END_POINTS };
