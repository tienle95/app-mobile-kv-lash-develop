import moment from 'moment';
import * as Yup from 'yup';

export const achiementsValidate = () => {
  return Yup.object().shape({
    title: Yup.string().nullable().required('createCandidate.achievements.requireTitle')
  });
};

export const experiencesValidate = () => {
  let experience = {};
  return Yup.object()
    .test({
      test: function () {
        experience = this.originalValue;
        return true;
      }
    })
    .shape({
      company: Yup.string().required('createCandidate.experiences.enterNameCompany'),
      startDate: Yup.date()
        .nullable()
        .test({
          message: 'createCandidate.experiences.chooseStartDateError',
          test: value => {
            if (!!experience?.endDate && !value) return false;
            return true;
          }
        })
        .test({
          message: 'createCandidate.experiences.startDateError',
          test: value => {
            const startMonthYear = `${moment(value).format('YYYY-MM')}-01`;
            const endMonthYear = `${moment(experience?.endDate).format('YYYY-MM')}-01`;
            return (
              moment(startMonthYear).isSame(endMonthYear) ||
              moment(startMonthYear).isBefore(endMonthYear)
            );
          }
        }),
      // .test({
      //   message: 'createCandidate.experiences.startDateBeforeNowError',
      //   test: value => {
      //     const startMonthYear = `${moment(value).format('YYYY-MM')}-01`;
      //     const timeNow = `${moment().format('YYYY-MM')}-01`;
      //     return moment(startMonthYear).isBefore(timeNow);
      //   }
      // }),
      endDate: Yup.date()
        .nullable()
        .test({
          message: 'createCandidate.experiences.chooseEndDateError',
          test: value => {
            if (!!experience?.startDate && !value) return false;
            return true;
          }
        })
        .test({
          message: 'createCandidate.experiences.endDateError',
          test: value => {
            const startMonthYear = `${moment(experience?.startDate).format('YYYY-MM')}-01`;
            const endMonthYear = `${moment(value).format('YYYY-MM')}-01`;
            return (
              moment(startMonthYear).isSame(endMonthYear) ||
              moment(endMonthYear).isAfter(startMonthYear)
            );
          }
        })
      // .test({
      //   message: 'createCandidate.experiences.startDateBeforeNowError',
      //   test: value => {
      //     const endMonthYear = `${moment(value).format('YYYY-MM')}-01`;
      //     const timeNow = `${moment().format('YYYY-MM')}-01`;
      //     return moment(endMonthYear).isBefore(timeNow) || moment(endMonthYear).isSame(timeNow);
      //   }
      // })
    });
};

export const educationsValidate = () => {
  let education = {};
  return Yup.object()
    .test({
      test: function () {
        education = this.originalValue;
        return true;
      }
    })
    .shape({
      institution: Yup.string()
        .nullable()
        .required('createCandidate.educations.enterNameInstitution'),
      studyType: Yup.string().nullable().required('createCandidate.educations.enterStudyType'),
      area: Yup.string().nullable().required('createCandidate.educations.enterArea'),
      startDate: Yup.date()
        .nullable()
        .test({
          message: 'createCandidate.educations.chooseStartDateError',
          test: value => {
            if (!!education?.endDate && !value) return false;
            return true;
          }
        })
        .test({
          message: 'createCandidate.educations.startDateError',
          test: value => {
            const startMonthYear = `${moment(value).format('YYYY-MM')}-01`;
            const endMonthYear = `${moment(education?.endDate).format('YYYY-MM')}-01`;

            return (
              moment(startMonthYear).isSame(endMonthYear) ||
              moment(startMonthYear).isBefore(endMonthYear)
            );
          }
        }),
      endDate: Yup.date()
        .nullable()
        .test({
          message: 'createCandidate.educations.chooseEndDateError',
          test: value => {
            if (!!education?.startDate && !value) return false;
            return true;
          }
        })
        .test({
          message: 'createCandidate.educations.endDateError',
          test: value => {
            const startMonthYear = `${moment(education?.startDate).format('YYYY-MM')}-01`;
            const endMonthYear = `${moment(value).format('YYYY-MM')}-01`;

            return (
              moment(startMonthYear).isSame(endMonthYear) ||
              moment(endMonthYear).isAfter(startMonthYear)
            );
          }
        })
    });
};

export const contactInfoValidate = () => {
  let phoneNumbers_ = [];
  let emailAddress_ = [];

  return Yup.object()
    .test({
      test: function () {
        const { phoneNumbers, emailAddress } = this.originalValue;
        phoneNumbers_ = phoneNumbers;
        emailAddress_ = emailAddress;
        return true;
      }
    })
    .shape({
      ims: Yup.array().nullable(),
      websites: Yup.array().nullable(),
      phoneNumbers: Yup.array(
        Yup.object().shape({
          value: Yup.string()
            .nullable()
            .required('createCandidate.contactInfo.enterPhone')
            .matches(/(\+84|84|0084|0)[0-9]{9}$/, 'createCandidate.contactInfo.enterPhone')
            .test({
              message: 'createCandidate.contactInfo.dulicatePhone',
              test: value => {
                let count = 0;
                for (let i = 0; i < phoneNumbers_?.length; i++) {
                  if (value?.trim() === phoneNumbers_?.[i].value?.trim()) {
                    count = count + 1;
                  }
                }
                return count < 2;
              }
            })
        })
      ),
      emailAddress: Yup.array(
        Yup.object().shape({
          value: Yup.string()
            .nullable()
            .required('createCandidate.contactInfo.enterEmail')
            .matches(/^\S+@\S+\.\S+$/, 'createCandidate.contactInfo.enterEmail')
            .test({
              message: 'createCandidate.contactInfo.dulicateEmail',
              test: value => {
                let count = 0;
                for (let i = 0; i < emailAddress_?.length; i++) {
                  if (value?.trim() === emailAddress_?.[i].value?.trim()) {
                    count = count + 1;
                  }
                }
                return count < 2;
              }
            })
        })
      )
    });
};

export const skillAndLanguageValidate = () => {
  let skills_ = [];
  let languages_ = [];
  return Yup.object()
    .test({
      test: function () {
        const { skills, languages } = this.originalValue;
        skills_ = skills;
        languages_ = languages;
        return true;
      }
    })
    .shape({
      skills: Yup.array(
        Yup.object()
          .shape({
            name: Yup.string().nullable()
          })
          .test({
            message: 'createCandidate.skillAndLanguages.emptySkills',
            test: value => {
              return !!value?.name;
            }
          })
          .test({
            message: 'createCandidate.skillAndLanguages.dulicateSkills',
            test: value => {
              let isDulicate = false;
              for (let i = 0; i < skills_?.length; i++) {
                if (
                  value?.id !== skills_[i]?.id &&
                  value?.name &&
                  skills_[i]?.name &&
                  skills_[i]?.name === value?.name
                ) {
                  isDulicate = true;
                  break;
                }
              }
              return !isDulicate;
            }
          })
      ),
      languages: Yup.array(
        Yup.object()
          .shape({
            language: Yup.string().nullable()
          })
          .test({
            message: 'createCandidate.skillAndLanguages.emptyLanguages',
            test: value => {
              return !!value?.language;
            }
          })
          .test({
            message: 'createCandidate.skillAndLanguages.dulicateLanguages',
            test: value => {
              let isDulicate = false;
              for (let i = 0; i < languages_?.length; i++) {
                if (
                  value?.id !== languages_[i]?.id &&
                  value?.language &&
                  languages_[i]?.language &&
                  languages_[i]?.language === value?.language
                ) {
                  isDulicate = true;
                  break;
                }
              }
              return !isDulicate;
            }
          })
      )
    });
};
