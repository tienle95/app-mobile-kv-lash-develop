import { img_empty } from 'assets/images';
import AppText from 'components/AppText';
import { scale } from 'constants/scale';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image';

const EmptyContent = ({ title, img, style, titleStyle }) => {
  return (
    <View style={[style, styles.container]}>
      <FastImage source={img || img_empty} style={styles.img} />
      <AppText style={[titleStyle, styles.title]}>{title || 'Không tìm thấy kết quả nào'}</AppText>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: scale(32)
  },
  img: {
    width: scale(250),
    height: scale(190)
  },
  title: {
    color: '#848589',
    fontSize: scale(14),
    textAlign: 'center',
    paddingTop: scale(24)
  }
});

export default EmptyContent;
