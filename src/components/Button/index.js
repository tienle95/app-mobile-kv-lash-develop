import { StyleSheet, TouchableOpacity } from 'react-native';
import React from 'react';

const ButtonCustom = props => {
  const { style, children, ...otherProps } = props;

  return (
    <TouchableOpacity {...otherProps} style={style || styles.buttonDefault}>
      {children}
    </TouchableOpacity>
  );
};

export default ButtonCustom;

const styles = StyleSheet.create({
  buttonDefault: {
    justifyContent: 'center',
    alignItems: 'center'
  }
});
