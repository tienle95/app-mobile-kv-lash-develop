// import { FONT_SIZE } from 'constants/appFonts';
import { FONT_FAMILY } from 'constants/appFont';
import { colors } from 'constants/colors';
import { DEVICE, scale } from 'constants/scale';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: hasBottomLine => ({
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: scale(16),
    paddingVertical: scale(13),
    backgroundColor: colors.bg_primary,
    borderBottomColor: colors.white_smoke,
    borderBottomWidth: hasBottomLine ? 1 : 0
    // marginTop: Platform.OS === 'android' ? scale(40) : 0
  }),
  title: {
    fontSize: scale(24),
    color: 'black',
    fontFamily: FONT_FAMILY.BOLD,
    paddingHorizontal: scale(16),
    width: DEVICE.WIDTH - scale(112),
    textAlign: 'center'
  },
  titleRight: {
    // fontSize: FONT_SIZE.Large,
    color: colors.text_yellow,
    // fontFamily: FONT_FAMILY.MEDIUM,
    paddingHorizontal: scale(16)
  },
  btnRight: {
    // fontFamily: FONT_FAMILY.REGULAR,
    // fontSize: FONT_SIZE.Normal,
    color: colors.light
  },
  iconLeft: {
    height: scale(24),
    width: scale(24)
  },
  logo: { width: scale(67), height: scale(32) },
  rightButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: scale(24),
    height: scale(24)
  },
  leftButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: scale(24),
    height: scale(24)
  }
});
export default styles;
