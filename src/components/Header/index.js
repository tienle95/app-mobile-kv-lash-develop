import AppText from 'components/AppText';
import React, { useCallback, useEffect } from 'react';
import { View, TouchableOpacity, Image, BackHandler } from 'react-native';
import styles from './styles';
import NavigationServices from 'utils/navigationServices';
import PropTypes from 'prop-types';
import { ic_back } from 'assets/icons';
import { isFunction } from 'lodash';
import FastImage from 'react-native-fast-image';
import { img_logoKvlash } from 'assets/images';

const Header = ({
  back,
  onBack,
  title,
  rightIcon,
  onPressRightIcon,
  titleRight,
  onPressRightTitle,
  styleTitleRight,
  getLayout,
  isHome,
  logo,
  hasBottomLine
}) => {
  const goBack = useCallback(() => {
    NavigationServices.goBack();
  }, []);

  useEffect(() => {
    const backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      BackHandler.removeEventListener('hardwareBackPress', () => {});
      if (onBack && isFunction(onBack)) {
        onBack();
      } else goBack();
      return true;
    });
    return () => backHandler.remove();
  }, [goBack, onBack]);

  return (
    <View style={styles.container(hasBottomLine)} onLayout={e => getLayout?.(e)}>
      {back && !isHome ? (
        <TouchableOpacity onPress={onBack && isFunction(onBack) ? onBack : () => goBack()}>
          <Image source={ic_back} style={styles.iconLeft} />
        </TouchableOpacity>
      ) : (
        <View style={styles.iconLeft} />
      )}
      {title && (
        <AppText numberOfLines={1} style={styles.title} translate>
          {title}
        </AppText>
      )}
      {logo && <FastImage style={styles.logo} source={logo || img_logoKvlash} />}

      {rightIcon ? (
        <TouchableOpacity onPress={() => onPressRightIcon?.()}>
          <FastImage source={rightIcon} style={styles.iconLeft} resizeMode="contain" />
        </TouchableOpacity>
      ) : titleRight ? (
        <TouchableOpacity onPress={() => onPressRightTitle?.()}>
          <AppText numberOfLines={1} style={[styles.titleRight, styleTitleRight]} translate>
            {titleRight}
          </AppText>
        </TouchableOpacity>
      ) : (
        <View style={styles.iconLeft} />
      )}
    </View>
  );
};

export default Header;

Header.propTypes = {
  back: PropTypes.bool,
  rightIcon: PropTypes.any,
  title: PropTypes.string
};

Header.defaultProps = {
  back: true
};
