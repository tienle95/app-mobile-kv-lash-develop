import React from 'react';
import { StyleProp, TextProps, TextStyle } from 'react-native';

interface AppTextProps extends TextProps {
  style?: StyleProp<TextStyle>;
  children?: any;
  /** @default: colors.text_black */
  color?: any;
  /**
   * S: 10, XS: 12, M: 14, L: 16, XL: 18, XXL: 20, X3L: 24, X4L: 32,
   * @default: M
   */
  size?: 'S' | 'XS' | 'M' | 'L' | 'XL' | 'XXL' | 'X3L' | 'X4L';
  weight?: 'light' | 'regular' | 'medium' | 'semi-bold' | 'bold';
  align?: 'auto' | 'center' | 'left' | 'right' | 'justify';
  uppercase?: boolean;
  lowercase?: boolean;
  line?: number;
}
declare const AppText: React.FC<AppTextProps>;

export default AppText;
