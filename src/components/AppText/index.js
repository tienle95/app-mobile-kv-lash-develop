import React, { memo, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
import { useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import { translate as trans } from 'src/i18n';
import { FONT_FAMILY } from 'constants/appFont';
import { scale } from 'constants/scale';
import { colors } from 'constants/colors';
import withStyle from '../../HOC/withStyle';

export const selectText = createSelector(
  state => state.settingReducer.lang,
  (_, children) => children,
  (_, children) => trans(children, { locale: _ || 'vi' })
);

const SIZES = {
  S: scale(10),
  XS: scale(12),
  M: scale(14),
  L: scale(16),
  XL: scale(18),
  XXL: scale(20),
  X3L: scale(24),
  X4L: scale(32)
};
const configFontSize = size => {
  if (size) {
    return SIZES[size];
  }
  return SIZES.M;
};

const AppText = ({
  children,
  style,
  size,
  weight,
  color,
  uppercase,
  align,
  lowercase,
  translate = false,
  line,
  ...props
}) => {
  const lang = useSelector(state => selectText(state, children));

  const styleContainer = useMemo(() => {
    const defaultStyle = {
      fontFamily: FONT_FAMILY.REGULAR,
      fontSize: configFontSize('M')
    };

    let fontSizeStyle = {
      fontSize: configFontSize(size)
    };

    const fontStyle =
      weight === 'bold'
        ? FONT_FAMILY.BOLD
        : weight === 'semi-bold'
        ? FONT_FAMILY.SEMI_BOLD
        : weight === 'light'
        ? FONT_FAMILY.LIGHT
        : weight === 'medium'
        ? FONT_FAMILY.MEDIUM
        : FONT_FAMILY.REGULAR;

    const styleF = () => {
      const result = {};
      if (uppercase) {
        return { ...result, textTransform: 'uppercase' };
      }
      if (lowercase) {
        return { ...result, textTransform: 'lowercase' };
      }
      return result;
    };
    return [defaultStyle, fontSizeStyle, styleF(), { fontFamily: fontStyle }, style];
  }, [lowercase, size, style, uppercase, weight]);

  return (
    <Text
      {...props}
      style={[
        { color: color ? color : colors.text_black, textAlign: align ? align : 'left' },
        line && { lineHeight: line },
        styleContainer
      ]}
      allowFontScaling={false}>
      {translate ? lang + '' : children}
    </Text>
  );
};

AppText.propTypes = {
  style: PropTypes.any,
  translate: PropTypes.bool,
  children: PropTypes.any
};

AppText.defaultProps = {
  translate: false
};

export default memo(withStyle(AppText));
