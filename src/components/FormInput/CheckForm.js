import AppText from 'components/AppText';
import StyledTouchable from 'components/StyledTouchable';
import PropsTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import { Image, StyleSheet } from 'react-native';
import { icCheckActive, icCheckInactive } from '../../assets/icons';
import { SPACING } from '../../constants/scale';

const CheckForm = ({ containerStyle, title, value, name, onChange, titleStyle, colorTitle }) => {
  const [checked, setChecked] = useState(value);

  const onPress = useCallback(() => {
    setChecked(prev => !prev);
    onChange(!checked);
  }, [onChange, checked]);
  useEffect(() => {
    setChecked(value);
  }, [value]);
  return (
    <StyledTouchable onPress={onPress} style={[styles.container, containerStyle]}>
      {checked ? <Image source={icCheckActive} /> : <Image source={icCheckInactive} />}
      <AppText style={[styles.title, titleStyle]} color={colorTitle}>
        {title}
      </AppText>
    </StyledTouchable>
  );
};

CheckForm.propTypes = {
  containerStyle: PropsTypes.object,
  title: PropsTypes.string,
  value: PropsTypes.bool,
  name: PropsTypes.string,
  onchange: PropsTypes.func,
  icChecked: PropsTypes.element,
  icUnCheck: PropsTypes.element,
  titleStyle: PropsTypes.object,
  colorTitle: PropsTypes.string
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: SPACING.Medium
  },
  title: {
    paddingLeft: SPACING.Normal,
    paddingVertical: SPACING.Normal
  }
});

export default React.memo(CheckForm);
