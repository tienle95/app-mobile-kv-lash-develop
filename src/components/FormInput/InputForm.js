import { FONT_FAMILY, FONT_SIZE } from 'constants/appFont';
import React, { useCallback, useRef, useState } from 'react';
import { Image, Platform, StyleSheet, TextInput, View } from 'react-native';
import { colors } from '../../constants/colors';
import { LINE_HEIGHT, scale, SPACING, verticalScale } from '../../constants/scale';
import { formatPhoneNumber } from '../../helpers/formatNumber';
import { removeEmoji } from '../../helpers/string';
import AppText from '../AppText';
import StyledTouchable from '../StyledTouchable';

const InputForm = props => {
  const {
    type,
    multiline,
    title,
    placeholderTxt,
    placeholder,
    value,
    upperCase,
    onChangeText,
    editable = true,
    autoCapitalize = 'none',
    keyboardType,
    item,
    maxLength,
    noBorder = false,
    hasExtend,
    borderBottomColor,
    errors,
    titleColor,
    onClearValue,
    isDelete,
    containerStyle,
    style,
    handleBlur,
    name,
    touched,
    typeInput,
    limit = 2000,
    note = false,
    prefix,
    onOutFocused,
    refInput,
    onFocus,
    ...rest
  } = props;
  const inputRef = useRef({
    isFocus: false,
    isValidated: false,
    hasValue: false
  });
  const [isFocus, setFocus] = useState(false);
  const position = useRef(0);
  const onFocusInput = useCallback(() => {
    onFocus?.(position.current);
    setFocus(true);
    inputRef.current.isFocus = true;
  }, [onFocus]);

  const onBlur = useCallback(() => {
    setFocus(false);
    inputRef.current.isFocus = false;
    handleBlur(name);
    if (typeof onOutFocused === 'function') {
      onOutFocused();
    }
  }, [handleBlur, name, onOutFocused]);

  const onChange = useCallback(
    text => {
      if ((prefix && typeInput === 'phone') || (prefix && typeInput === 'number')) {
        const phoneFormat = formatPhoneNumber(text);
        let content = '';
        if (phoneFormat) {
          content = prefix + phoneFormat;
        } else {
          content = phoneFormat;
        }
        onChangeText(name)(removeEmoji(content));
      } else if (typeInput === 'phone' || typeInput === 'number') {
        const phoneFormat = formatPhoneNumber(text);
        onChangeText(name)(removeEmoji(phoneFormat));
      } else {
        onChangeText(name)(removeEmoji(text));
      }
    },
    [prefix, onChangeText, name, typeInput]
  );

  const onLayout = useCallback(event => {
    const layout = event.nativeEvent.layout;
    position.current = layout.y;
  }, []);
  return (
    <View style={[styles.container, containerStyle]} onLayout={onLayout}>
      <View style={[styles.row, { justifyContent: 'space-between' }]}>
        {!!title && (
          <View style={styles.row}>
            <AppText
              weight="semi-bold"
              color={titleColor || colors.text_black}
              style={[styles.title, type === 'textarea' && { paddingBottom: 0 }]}>
              {title}
              {rest?.isRequired ? <AppText color={colors.radicalRed}>*</AppText> : null}
            </AppText>
          </View>
        )}
        {type === 'textarea' ? (
          <AppText color={colors.silver} style={styles.length}>
            {value.trim().length}/{limit}
          </AppText>
        ) : null}
      </View>
      {/* {note && isTooltipVisible && (
        <View style={styles.content}>
          <AppText style={styles.txtContent} color={CUSTOM_COLOR.White}>
            {note}
          </AppText>
        </View>
      )} */}
      <View
        style={type === 'textarea' ? {} : styles.inputWrapper}
        pointerEvents={type === 'select' || type === 'address-picker' ? 'none' : 'auto'}>
        {type === 'textarea' ? (
          <>
            <TextInput
              ref={ref => refInput(ref)}
              blurOnSubmit={false}
              placeholder={placeholder}
              value={value}
              onChangeText={text => onChangeText(name)(removeEmoji(text))}
              style={[
                styles.inputMultiContainer,
                {
                  borderColor: errors[name] && touched[name] ? colors.app_red : colors.gallery,
                  backgroundColor: editable ? colors.white : colors.gallery
                },
                style
              ]}
              placeholderTextColor={colors.silver}
              onFocus={onFocusInput}
              onBlur={onBlur}
              editable={editable}
              autoCorrect={false}
              autoCapitalize={autoCapitalize}
              keyboardType={keyboardType || 'default'}
              maxLength={maxLength}
              multiline={true}
              {...rest}
            />
          </>
        ) : (
          <View
            style={[
              styles.viewInput,
              // isFocus && styles.containerFocused,
              touched[name] && errors[name] && { borderColor: colors.app_red }
            ]}>
            {rest?.iconLeft ? (
              <StyledTouchable onPress={rest?.onLeftIconPress} style={styles.iconContainer}>
                <Image
                  source={rest?.iconLeft}
                  style={[styles.iconStyle, rest?.iconLeftStyle]}
                  resizeMode="contain"
                />
              </StyledTouchable>
            ) : null}
            <TextInput
              ref={ref => refInput(ref)}
              blurOnSubmit={false}
              placeholder={placeholder}
              value={value}
              onChangeText={onChange}
              style={[
                styles.inputContainer,
                !value && styles.placeholderStyle,
                {
                  borderColor: errors[name] && touched[name] ? colors.app_red : colors.gallery,
                  backgroundColor: editable
                    ? colors.white
                    : rest?.notUseDefaultDisableColor
                    ? colors.white
                    : colors.gallery
                },
                style
              ]}
              placeholderTextColor={colors.silver}
              onFocus={onFocusInput}
              onBlur={onBlur}
              editable={editable}
              autoCorrect={false}
              autoCapitalize={autoCapitalize}
              keyboardType={keyboardType || 'default'}
              maxLength={maxLength}
              underlineColorAndroid={'transparent'}
              {...rest}
            />
            {rest?.iconRight ? (
              <StyledTouchable onPress={rest?.onRightIconPress} style={styles.iconContainer}>
                <Image
                  source={rest?.iconRight}
                  style={[styles.iconStyle, rest?.iconRightStyle]}
                  resizeMode="contain"
                />
              </StyledTouchable>
            ) : null}
          </View>
        )}
        {/* {isFocus && isDelete && value?.trim()?.length > 0 && (
          <StyledTouchable style={styles.iconDelete} onPress={onClearValue}>
            <IcDeleteInput />
          </StyledTouchable>
        )} */}
      </View>
      {!noBorder && type !== 'textarea' && (
        <View
          style={{
            backgroundColor:
              errors[name] && touched[name]
                ? colors.bright_red
                : isFocus
                ? colors.gray
                : colors.alto,
            ...styles.divider
          }}
        />
      )}

      {touched[name] && errors[name] ? (
        <AppText color={colors.app_red} style={styles.errorText} mx-8>
          {errors[name]}
        </AppText>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  flex: {
    flex: 1
  },
  container: {
    marginTop: SPACING.Medium,
    backgroundColor: colors.white
  },
  inputContainer: {
    flex: 1,
    color: colors.text_black,
    fontSize: FONT_SIZE.small,
    fontFamily: FONT_FAMILY.REGULAR,
    height: verticalScale(48),
    paddingHorizontal: 0,
    paddingVertical: 0,
    marginVertical: 0,
    textAlignVertical: 'center',
    borderRadius: 11
  },
  inputMultiContainer: {
    fontSize: FONT_SIZE.small,
    fontFamily: FONT_FAMILY.REGULAR,
    color: colors.text_black,
    borderWidth: 1,
    // padding: scale(12),
    paddingHorizontal: 0,
    paddingTop: scale(12),
    height: scale(142),
    textAlignVertical: Platform.select({ android: 'top', ios: 'undefined' })
  },
  title: {
    lineHeight: LINE_HEIGHT.SubHead,
    paddingBottom: SPACING.Tiny
  },
  divider: {
    height: 1
  },
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  placeholder: {
    color: colors.granite_gray
  },
  errorText: {
    justifyContent: 'flex-start',
    lineHeight: LINE_HEIGHT.SubHead
  },
  errorTextWrapper: {
    marginTop: scale(2),
    justifyContent: 'center'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputExtend: {
    marginBottom: SPACING.Normal
  },
  inputView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    backgroundColor: colors.white
  },
  textareaMark: {
    position: 'absolute',
    right: 0,
    bottom: 0
  },
  maxLength: {
    lineHeight: LINE_HEIGHT.SubHead,
    textAlign: 'right'
  },
  radioBox: {
    paddingRight: SPACING.Medium
  },
  radioBoxesContainer: {},
  iconDelete: {
    padding: scale(8),
    margin: -scale(8)
  },
  viewInput: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    // borderColor: colors.gallery,
    // borderWidth: 1,
    // borderRadius: 12,
    backgroundColor: colors.white
    // paddingBottom: 0.5
  },
  length: {},
  noteText: {
    fontStyle: 'italic'
  },
  inputWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.white
  },
  placeholderStyle: {},
  content: {
    position: 'absolute',
    backgroundColor: colors.mako,
    paddingHorizontal: SPACING.Medium,
    paddingVertical: SPACING.Normal,
    borderTopLeftRadius: scale(8),
    borderTopRightRadius: scale(8),
    borderBottomLeftRadius: scale(8),
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    elevation: 10,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    top: scale(-50)
  },
  iconStyle: {
    width: scale(24),
    height: scale(24)
  },
  iconContainer: {
    height: verticalScale(48),
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerFocused: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.15,
    shadowRadius: 3.84,
    elevation: 3
  }
});

export default React.memo(InputForm);
