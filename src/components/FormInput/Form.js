import { Formik } from 'formik';
import React, { useImperativeHandle, useMemo, useRef } from 'react';
import { Keyboard, StyleSheet } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { ic_arrowRight } from '../../assets/icons';
import { colors } from '../../constants/colors';
import SCREENS from '../../constants/screens';
import NavigationServices from '../../utils/navigationServices';
import StyledTouchable from '../StyledTouchable';
// import CheckForm from './CheckForm';
import DateForm from './DateForm';
import InputForm from './InputForm';

const Form = React.forwardRef(
  (
    {
      initialValues,
      onSubmit,
      validationSchema,
      genValues,
      body,
      currentValue,
      onChange,
      view,
      customInputStyle,
      onScrollTo,
      enableReinitialize = false,
      onFocusInput,
      ...props
    },
    ref
  ) => {
    const refs = useRef([]);
    const selectRef = useRef([]);
    const Container = useMemo(() => (view ? view : KeyboardAwareScrollView), [view]);
    const closeAllSelectForm = key => {
      if (selectRef.current.length) {
        selectRef.current?.map(select => {
          if (select?.isOpen) {
            select?.closeDropdown();
          }
        });
      }
      if (refs.current.length) {
        refs.current?.map(input => {
          if (input?.name !== key) {
            input?.setTooltipVisible?.(false);
          }
        });
      }
    };
    return (
      <Formik
        {...props}
        initialValues={initialValues}
        innerRef={ref}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
        enableReinitialize={enableReinitialize}>
        {({
          values,
          handleChange,
          setFieldValue,
          setFieldTouched,
          errors,
          touched,
          dirty,
          handleSubmit,
          handleBlur,
          setFieldError,
          handleReset
        }) => {
          useImperativeHandle(ref, () => ({
            ...ref.current,
            closeAllSelectForm,
            reset: handleReset,
            dirty
          }));
          currentValue && currentValue(values);
          return (
            <Container
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              keyboardShouldPersistTaps="handled"
              // KeyboardAwareScrollView props
              scrollIndicatorInsets={{ right: 1 }}
              enableOnAndroid={true}
              contentContainerStyle={
                Container === KeyboardAwareScrollView ? { paddingHorizontal: 1, flexGrow: 1 } : {}
              }
              extraHeight={250}>
              {genValues.map((item, index) => {
                if (item.type === 'input' || item.type === 'textarea') {
                  return (
                    <InputForm
                      {...item}
                      refInput={element => {
                        const isExist = refs?.current?.find(e => e?.name === element?.name);
                        if (element !== null && element !== undefined && !isExist) {
                          element.name = item.name;
                          refs.current.push(element);
                        }
                      }}
                      key={item.name}
                      name={item.name}
                      title={item.title}
                      placeholder={item.placeholder}
                      style={[styles.input, customInputStyle]}
                      prefix={item?.prefix}
                      maxLength={item?.maxLength}
                      value={values[item.name]}
                      onFocus={position => {
                        onScrollTo?.(position + 250, index === genValues.length - 1);
                        onFocusInput?.();
                        closeAllSelectForm(item.name);
                      }}
                      onChangeText={name => text => {
                        handleChange(name)(text);
                        typeof onChange === 'function' && onChange(name, text);
                      }}
                      isDelete={item.isDelete}
                      type={item.type}
                      numberOfLines={3}
                      typeInput={item.typeInput}
                      note={item.note}
                      returnKeyType={item.returnKeyType || 'done'}
                      keyboardType={
                        item.typeInput === 'phone'
                          ? 'number-pad'
                          : item.typeInput === 'number'
                          ? 'number-pad'
                          : 'default'
                      }
                      onClearValue={() => setFieldValue(item.name, '')}
                      onSubmitEditing={() => {
                        if (refs?.current?.length === index + 1) {
                          Keyboard.dismiss();
                        }
                        if (
                          genValues[index + 1]?.disabled ||
                          (genValues[index + 1]?.type !== 'input' &&
                            genValues[index + 1]?.type !== 'textarea')
                        ) {
                          refs?.current[index + 2] && refs?.current[index + 2]?.focus?.();
                        } else {
                          refs?.current[index + 1] && refs?.current[index + 1]?.focus?.();
                        }
                      }}
                      onOutFocused={() => {
                        if (item?.isOutFocused) {
                          setFieldTouched(item.name, true);
                        }
                        if (item?.isTrim) {
                          setFieldValue(item.name, values[item.name]?.trim());
                        }
                      }}
                      editable={!item.disabled}
                      {...{ handleBlur, errors, touched }}
                    />
                  );
                }
                if (item.type === 'date') {
                  return (
                    <DateForm
                      {...item}
                      key={item.name}
                      title={item.title}
                      placeholder={item.placeholder}
                      name={item.name}
                      value={values[item.name]}
                      mode={item?.mode}
                      onConfirm={value => setFieldValue(item.name, value)}
                      setCloseDate={() => {
                        if (!values[item.name]) {
                          setFieldTouched(item.name, true);
                        }
                      }}
                      {...{ handleBlur, errors, touched }}
                    />
                  );
                }
                if (item.type === 'select') {
                  return (
                    <StyledTouchable
                      onPress={() => {
                        NavigationServices.navigate(item?.screen, {
                          ...item?.additionalParams
                        });
                      }}>
                      <InputForm
                        {...item}
                        refInput={element => {
                          const isExist = refs?.current?.find(e => e?.name === element?.name);
                          if (element !== null && element !== undefined && !isExist) {
                            element.name = item.name;
                            refs.current.push(element);
                          }
                        }}
                        key={item.name}
                        name={item.name}
                        title={item.title}
                        placeholder={item.placeholder}
                        style={[styles.input, customInputStyle, { backgroundColor: colors.white }]}
                        prefix={item?.prefix}
                        maxLength={item?.maxLength}
                        value={values[item.name]}
                        onFocus={position => {
                          onFocusInput?.();
                        }}
                        onChangeText={name => text => {
                          handleChange(name)(text);
                          typeof onChange === 'function' && onChange(name, text);
                        }}
                        isDelete={item.isDelete}
                        type={item.type}
                        numberOfLines={3}
                        typeInput={item.typeInput}
                        note={item.note}
                        onClearValue={() => setFieldValue(item.name, '')}
                        editable={false}
                        iconRight={ic_arrowRight}
                        iconRightStyle={{ tintColor: colors.disable_color }}
                        selection={{ start: 0 }}
                        {...{ handleBlur, errors, touched }}
                      />
                    </StyledTouchable>
                  );
                }
                if (item.type === 'address-picker') {
                  return (
                    <StyledTouchable
                      onPress={
                        item?.onPress
                          ? item?.onPress
                          : () => {
                              NavigationServices.navigate(
                                item?.screen || SCREENS.PICK_ADDRESS,
                                item?.additionalParams
                              );
                            }
                      }>
                      <InputForm
                        {...item}
                        refInput={element => {}}
                        key={item.name}
                        name={item.name}
                        title={item.title}
                        placeholder={item.placeholder}
                        style={[styles.input, customInputStyle, { backgroundColor: colors.white }]}
                        prefix={item?.prefix}
                        maxLength={item?.maxLength}
                        value={values[item.name]}
                        onFocus={position => {
                          onFocusInput?.();
                        }}
                        onChangeText={name => text => {
                          handleChange(name)(text);
                          typeof onChange === 'function' && onChange(name, text);
                        }}
                        isDelete={item.isDelete}
                        type={item.type}
                        numberOfLines={3}
                        typeInput={item.typeInput}
                        note={item.note}
                        onClearValue={() => setFieldValue(item.name, '')}
                        editable={false}
                        iconRight={ic_arrowRight}
                        iconRightStyle={{ tintColor: colors.disable_color }}
                        selection={{ start: 0 }}
                        {...{ handleBlur, errors, touched }}
                      />
                    </StyledTouchable>
                  );
                }
                {
                  /* if (item.type === 'checkbox') {
                  return (
                    <CheckForm
                      key={item.name}
                      title={item.title}
                      value={values[item.name]}
                      onChange={value => {
                        typeof onChange === 'function' && onChange(item.name, value);
                        setFieldValue(item.name, value);
                      }}
                      {...{ handleBlur, errors, touched }}
                    />
                  );
                } */
                }
              })}
              {body && body(handleSubmit, errors)}
            </Container>
          );
        }}
      </Formik>
    );
  }
);

const styles = StyleSheet.create({
  input: {
    borderColor: '#fff',
    borderWidth: 0
  }
});

export default React.memo(Form);
