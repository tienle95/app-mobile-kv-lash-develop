import React from 'react';
import { View, StyleSheet, Keyboard } from 'react-native';
import PropTypes from 'prop-types';
import ModalDatePicker from 'components/Modal/ModalDatePicker';
import moment from 'moment';
import AppText from 'components/AppText';
import StyledTouchable from 'components/StyledTouchable';
import { colors } from '../../constants/colors';
import FastImage from 'react-native-fast-image';
import { ic_arrowRight, ic_calendar_new } from '../../assets/icons';
import { LINE_HEIGHT, scale, verticalScale } from '../../constants/scale';

const optionsDefault = {
  title: ''
};

const DateForm = ({
  mode = 'date',
  title,
  placeholder,
  errors,
  touched,
  onConfirm,
  value,
  name,
  setCloseDate,
  noBorder = false,
  isFocus,
  minimumDate,
  maximumDate,
  ...rest
}) => {
  const [open, setOpen] = React.useState(false);

  const onOpen = () => {
    Keyboard.dismiss();
    setOpen(true);
  };

  return (
    <View style={styles.container}>
      {title ? (
        <AppText mb-8 style={styles.title} weight="semi-bold" color={colors.text_black}>
          {title}
          {rest?.isRequired ? <AppText color={colors.radicalRed}>*</AppText> : null}
        </AppText>
      ) : null}
      <StyledTouchable
        style={[
          styles.input,
          {
            borderColor: errors[name] && touched[name] ? colors.radicalRed : colors.gallery
          }
        ]}
        onPress={onOpen}>
        <AppText color={value ? colors.text_black : colors.silver}>
          {value
            ? mode === 'date'
              ? moment(value).format('DD/MM/YYYY')
              : moment(value).format('HH:mm')
            : placeholder}
        </AppText>
        <FastImage
          source={ic_arrowRight}
          style={{ width: scale(24), height: scale(24) }}
          tintColor={colors.disable_color}
        />
      </StyledTouchable>
      {touched[name] && errors[name] ? (
        <AppText size="S" color={colors.radicalRed} style={styles.errorText}>
          {errors[name]}
        </AppText>
      ) : null}
      <ModalDatePicker
        maximumDate={maximumDate}
        minimumDate={minimumDate}
        mode={mode}
        title={placeholder}
        value={value}
        onConfirm={isDate => onConfirm(isDate)}
        isVisible={open}
        handleCancel={() => {
          setOpen(false);
          setCloseDate();
        }}
      />
      {!noBorder && (
        <View
          style={{
            backgroundColor:
              errors[name] && touched[name]
                ? colors.bright_red
                : isFocus
                ? colors.gray
                : colors.alto,
            ...styles.divider
          }}
        />
      )}
    </View>
  );
};

DateForm.propTypes = {
  inputStyle: PropTypes.object,
  title: PropTypes.string,
  errors: PropTypes.any,
  touched: PropTypes.any,
  onConfirm: PropTypes.func,
  value: PropTypes.any,
  name: PropTypes.string
};

DateForm.defaultProps = {
  title: optionsDefault.title
};

const styles = StyleSheet.create({
  container: {
    marginTop: scale(16)
  },
  input: {
    height: verticalScale(56),
    borderRadius: scale(16),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
    // paddingHorizontal: scale(12)
  },
  title: {},
  errorText: {
    justifyContent: 'flex-start',
    // color: TEXT_COLOR.Flamingo,
    // fontSize: FONT_SIZE.SubHead,
    lineHeight: LINE_HEIGHT.SubHead
    // fontFamily: FONT_FAMILY.REGULAR,
  },
  divider: {
    height: 1
  }
});

export default React.memo(DateForm);
