import { FONT_FAMILY, FONT_SIZE } from 'constants/appFont';
import { colors } from 'constants/colors';
import { LINE_HEIGHT, scale, SPACING } from 'constants/scale';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  flex: {
    flex: 1
  },
  container: {
    marginTop: SPACING.Medium
  },
  inputContainer: {
    color: colors.mine_shaft,
    fontSize: FONT_SIZE.default,
    paddingLeft: 0,
    paddingVertical: 0,
    marginVertical: 0,
    textAlignVertical: 'center',
    padding: 0,
    flex: 1,
    // fontFamily: FONT_FAMILY.REGULAR,
    height: scale(45)
  },
  errorInputContainer: {
    borderColor: colors.carmine_pink
  },
  dateInput: {
    paddingLeft: 0,
    paddingVertical: 0,
    marginVertical: 0,
    textAlignVertical: 'center'
  },
  title: {
    // marginBottom: scale(8),
    lineHeight: LINE_HEIGHT.SubHead,
    paddingBottom: SPACING.Normal,
    color: colors.dark,
    // fontFamily: FONT_FAMILY.REGULAR,
    fontSize: FONT_SIZE.regular
  },
  divider: {
    marginTop: scale(11),
    height: 1
  },
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  comboBoxContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: LINE_HEIGHT.BodyText
  },
  textComboBox: {
    lineHeight: LINE_HEIGHT.BodyText,
    paddingLeft: 0
  },
  errorText: {
    justifyContent: 'flex-start',
    lineHeight: LINE_HEIGHT.SubHead
  },
  errorTextWrapper: {
    marginTop: scale(2),
    justifyContent: 'center'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputExtend: {
    marginBottom: SPACING.Normal
  },
  inputView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    backgroundColor: '#fff'
  },
  textareaMark: {
    position: 'absolute',
    right: 0,
    bottom: 0
  },
  maxLength: {
    lineHeight: LINE_HEIGHT.SubHead,
    textAlign: 'right'
  },
  radioBox: {
    paddingRight: SPACING.Medium
  },
  iconDelete: {
    marginLeft: scale(5)
  },
  inputWrapper: {
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center'
  },
  starRequire: {
    color: colors.carmine_pink,
    marginLeft: SPACING.Small,
    marginTop: -SPACING.Normal
  },
  btnDeleteItem: {
    borderRadius: scale(6),
    justifyContent: 'center',
    alignItems: 'center',
    padding: scale(8),
    backgroundColor: colors.misty_rose,
    marginHorizontal: scale(8)
  },
  iconDeleteItem: {
    height: scale(20),
    width: scale(20)
  },
  txtError: {
    // fontFamily: FONT_FAMILY.BOLD,
    color: colors.carmine_pink,
    fontSize: scale(14),
    lineHeight: scale(24)
  }
});

export default styles;
