import React, { useRef, useCallback, useState } from 'react';
import { View, TextInput, TouchableOpacity, Image } from 'react-native';

import AppText from '../AppText';
import styles from './styles';
// import { IcDeleteInput } from '~assets/icons';
import StyledTouchable from '../StyledTouchable';
import { colors } from 'constants/colors';
import { ic_delete } from 'assets/icons';

const Input = props => {
  const {
    refInput,
    multiline = false,
    title,
    placeholder,
    value,
    upperCase,
    onChangeText,
    editable,
    autoCapitalize = 'none',
    keyboardType,
    item,
    maxLength,
    errorText,
    titleColor,
    onClearValue,
    isDelete,
    containerStyle,
    style,
    sizeButtonDelete = 16,
    onPress,
    isRequire = false,
    isDeleteItem = false,
    onPressDeleteItem = () => {},
    ...rest
  } = props;

  const inputRef = useRef({
    isFocus: false,
    isValidated: false,
    hasValue: false
  });

  const [isFocus, setFocus] = useState(false);

  const onFocus = useCallback(() => {
    if (typeof onPress === 'function') {
      onPress();
    } else {
      setFocus(true);
      inputRef.current.isFocus = true;
    }
  }, [onPress]);

  const onBlur = useCallback(() => {
    setFocus(false);
    inputRef.current.isFocus = false;
  }, []);

  const onPressDeleteItemInput = useCallback(() => {
    onPressDeleteItem?.(item?.id);
  }, [item?.id, onPressDeleteItem]);

  return (
    <>
      <View style={[styles.container, containerStyle]}>
        <View style={[styles.row, { justifyContent: 'space-between' }]}>
          {title ? (
            <View style={styles.row}>
              <AppText translate style={[styles.title, !!titleColor && { color: titleColor }]}>
                {title}
              </AppText>
              {isRequire && <AppText style={styles.starRequire}>*</AppText>}
            </View>
          ) : null}
        </View>
        <View style={styles.inputWrapper}>
          <TextInput
            ref={refInput}
            blurOnSubmit={false}
            placeholder={placeholder}
            value={value}
            onChangeText={text => onChangeText(upperCase ? text.toUpperCase() : text)}
            style={[styles.inputContainer, style, !!errorText && styles.errorInputContainer]}
            placeholderTextColor={colors.silver}
            onFocus={onFocus}
            onBlur={onBlur}
            editable={editable}
            autoCorrect={false}
            autoCapitalize={autoCapitalize}
            keyboardType={keyboardType || 'default'}
            maxLength={maxLength}
            multiline={multiline}
            {...rest}
          />
          {isDeleteItem && (
            <TouchableOpacity onPress={onPressDeleteItemInput} style={styles.btnDeleteItem}>
              <Image style={styles.iconDeleteItem} source={ic_delete} />
            </TouchableOpacity>
          )}
          {isDelete && !!value && (
            <StyledTouchable style={styles.iconDelete} onPress={onClearValue}>
              {/* <IcDeleteInput width={sizeButtonDelete} height={sizeButtonDelete} /> */}
            </StyledTouchable>
          )}
        </View>
        {errorText && (
          <AppText translate style={styles.txtError}>
            {errorText}
          </AppText>
        )}
      </View>
    </>
  );
};

export default React.memo(Input);
