import { StyleProp, ViewStyle } from 'react-native';
declare interface ModalOptions {
  title: string;
  content: string;
  hasCancel: boolean;
  confirmText: string;
  cancelText: string;
  onConfirm: () => void;
  onCancel: () => void;
  customContent: any;
  footerWrapperStyle: StyleProp<ViewStyle>;
  invertButtonColor: boolean;
  modalHeader: any;
  modalFooter: any;
  closeOnBackdropPress: boolean;
  titleColor: any;
  contentColor: any;
  onModalHide: () => void;
  type: 'success' | 'error' | 'warning';
}
declare const showModal: (options: ModalOptions) => void;
export { showModal };
