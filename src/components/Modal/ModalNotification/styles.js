import { StyleSheet } from 'react-native';
import { DEVICE, scale, verticalScale } from '../../../constants/scale';
import { FONT_FAMILY, FONT_SIZE } from '../../../constants/appFont';
import { colors } from '../../../constants/colors';
const styles = StyleSheet.create({
  modalWrapper: {
    flex: 1,
    alignItems: 'center'
  },
  modalContainer: {
    width: DEVICE.WIDTH,
    position: 'absolute',
    bottom: verticalScale(-22),
    backgroundColor: colors.white,
    borderTopLeftRadius: scale(20),
    borderTopRightRadius: scale(20),
    overflow: 'hidden',
    paddingHorizontal: scale(20),
    paddingVertical: scale(24)
  },
  modalHeader: {
    backgroundColor: colors.white,
    justifyContent: 'center',
    marginBottom: scale(16),
    alignItems: 'center'
  },
  modalHeaderTitle: {
    color: colors.black,
    fontSize: FONT_SIZE.mediumLarge
  },
  modalBody: {
    alignItems: 'center'
  },
  modalButtonArea: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: scale(16),
    width: DEVICE.WIDTH - scale(32),
    alignSelf: 'center'
  },
  modalFooter: {
    marginTop: scale(16),
    justifyContent: 'center',
    alignItems: 'center'
  },
  proceedBtnTitle: {
    color: colors.white,
    fontSize: FONT_SIZE.regular
  },
  noCancelBtn: {
    justifyContent: 'center',
    paddingLeft: 0,
    paddingRight: 0
  },
  notiContent: {
    width: '100%',
    textAlign: 'center'
  },
  confirmText: {
    color: colors.white,
    fontSize: scale(16),
    lineHeight: scale(24),
    fontFamily: FONT_FAMILY.BOLD
  },
  confirmBtn: {
    justifyContent: 'center',
    height: scale(48),
    width: DEVICE.WIDTH - scale(32),
    borderRadius: scale(8),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.043,
    shadowRadius: 1,
    elevation: 2,
    backgroundColor: colors.primary_color
  },
  cancelTxt: {
    color: '#16B57F',
    fontSize: FONT_SIZE.regular,
    fontFamily: FONT_FAMILY.BOLD
  },
  modalImage: {
    width: scale(72),
    height: scale(64)
  }
});
export default styles;
