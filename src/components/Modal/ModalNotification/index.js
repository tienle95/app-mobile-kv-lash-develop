import React, { useEffect, useState } from 'react';
import { DeviceEventEmitter, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import Modal from 'react-native-modal';
import { iconError, iconSuccess } from '../../../assets/icons';
import { scale } from '../../../constants/scale';
import { colors } from '../../../constants/colors';
import AppButton from '../../AppButton';
import AppText from '../../AppText';
import styles from './styles';

const optionDefault = {
  title: '',
  content: '',
  hasCancel: false,
  confirmText: 'Confirm',
  cancelText: 'Cancel',
  onConfirm: () => {},
  onCancel: () => {},
  customContent: null,
  footerWrapperStyle: null,
  invertButtonColor: false,
  modalHeader: null,
  modalFooter: null,
  closeOnBackdropPress: false,
  titleColor: colors.gray,
  contentColor: colors.gray,
  onModalHide: () => {},
  type: 'normal',
  confirmBtnColor: colors.primary_color
};

const defaultSuccessOptions = {
  modalHeader: (
    <FastImage
      source={iconSuccess}
      style={{ width: scale(70), height: scale(70) }}
      resizeMode="contain"
    />
  ),
  titleColor: colors.app_green,
  confirmBtnColor: colors.app_green
};

const defaultErrorOptions = {
  modalHeader: (
    <FastImage
      source={iconError}
      style={{ width: scale(70), height: scale(70) }}
      resizeMode="contain"
    />
  ),
  titleColor: colors.app_red,
  confirmBtnColor: colors.app_red
};

const defaultWarningOptions = {
  modalHeader: (
    <FastImage
      source={iconError}
      style={{ width: scale(70), height: scale(70) }}
      resizeMode="contain"
      tintColor={'orange'}
    />
  ),
  titleColor: 'orange',
  confirmBtnColor: 'orange'
};

const ModalNotification = () => {
  const [isVisible, setIsVisible] = useState(false);
  const [option, setOption] = useState(optionDefault);

  useEffect(() => {
    const listener = DeviceEventEmitter.addListener('MAIN_MODAL_SHOW', options => {
      setIsVisible(true);
      if (options.type === 'success') {
        setOption({ ...optionDefault, ...defaultSuccessOptions, ...options });
      } else if (options.type === 'error') {
        setOption({ ...optionDefault, ...defaultErrorOptions, ...options });
      } else if (options.type === 'warning') {
        setOption({ ...optionDefault, ...defaultWarningOptions, ...options });
      } else {
        setOption({ ...optionDefault, ...options });
      }
    });
    return () => {
      listener.remove();
    };
  }, []);
  useEffect(() => {
    const listener = DeviceEventEmitter.addListener('HIDE_MODAL_SHOW', () => {
      setIsVisible(false);
    });
    return () => {
      listener.remove();
    };
  }, []);
  const onConfirm = () => {
    setIsVisible(false);
    setTimeout(() => {
      option.onConfirm && option.onConfirm();
    }, 600);
  };
  const onCancel = () => {
    setIsVisible(false);
    setTimeout(() => {
      option.onCancel && option.onCancel();
    }, 600);
  };
  return (
    <Modal
      statusBarTranslucent
      animationIn={'slideInUp'}
      animationInTiming={400}
      onModalHide={option?.onModalHide}
      isVisible={isVisible}
      onBackdropPress={option.closeOnBackdropPress ? () => setIsVisible(false) : () => null}
      style={styles.modalWrapper}>
      <View style={styles.modalContainer}>
        {option?.modalHeader ? (
          typeof option.modalHeader === 'function' ? (
            <View style={styles.modalHeader}>{option.modalHeader()}</View>
          ) : (
            <View style={styles.modalHeader}>{option.modalHeader}</View>
          )
        ) : null}
        <View style={styles.modalBody}>
          <AppText color={option?.titleColor} size="X3L" weight={'bold'} mb-8>
            {option?.title}
          </AppText>
          {option.content ? (
            <AppText color={option?.contentColor} style={styles.notiContent}>
              {option.content}
            </AppText>
          ) : null}
          {option.customContent ? option?.customContent : null}
        </View>
        <View style={[styles.modalButtonArea, option?.footerWrapperStyle]}>
          {/* {option.hasCancel && (
            <AppButton
              onPress={onCancel}
              disableIfNoInternet={false}
              throttleTime={0}
              title={option.cancelText}
              textStyle={option.invertButtonColor ? styles.confirmText : styles.cancelTxt}
              containerStyle={
                option.invertButtonColor
                  ? styles.confirmBtn(option.hasCancel)
                  : styles.cancelBtn(option.hasCancel)
              }
            />
          )} */}
          <AppButton
            onPress={onConfirm}
            throttleTime={0}
            textStyle={option.invertButtonColor ? styles.cancelTxt : styles.confirmText}
            style={styles.confirmBtn}>
            <AppText weight="semi-bold" color={colors.white}>
              {option.confirmText}
            </AppText>
          </AppButton>
        </View>
        {option?.modalFooter ? (
          typeof option.modalFooter === 'function' ? (
            <View style={styles.modalFooter}>{option.modalFooter()}</View>
          ) : (
            <View style={styles.modalFooter}>{option.modalFooter}</View>
          )
        ) : null}
      </View>
    </Modal>
  );
};

export const showModal = (options = optionDefault) => {
  DeviceEventEmitter.emit('MAIN_MODAL_SHOW', options);
};

export const hideModal = (options = optionDefault) => {
  DeviceEventEmitter.emit('MAIN_MODAL_HIDE', options);
};

export default React.memo(ModalNotification);
