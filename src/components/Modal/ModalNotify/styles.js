import { FONT_FAMILY, FONT_SIZE } from 'constants/appFont';
import { colors } from 'constants/colors';
import { DEVICE, scale } from 'constants/scale';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.bg_modal
  },
  content: {
    borderTopLeftRadius: scale(16),
    borderTopRightRadius: scale(16),
    backgroundColor: colors.white,
    paddingTop: scale(12),
    paddingBottom: scale(16),
    position: 'absolute',
    bottom: 0,
    width: DEVICE.WIDTH
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: scale(16)
  },
  icClose: {
    width: scale(20),
    height: scale(20)
  },
  button: {
    marginHorizontal: scale(8),
    marginTop: scale(24)
  },
  title: {
    marginTop: scale(16),
    color: colors.yankees_blue,
    fontSize: FONT_SIZE.large,
    fontFamily: FONT_FAMILY.BOLD,
    textAlign: 'center',
    lineHeight: scale(24),
    marginHorizontal: scale(16)
  },
  description: {
    marginTop: scale(4),
    color: colors.auro_metal_saurus,
    fontSize: FONT_SIZE.normal,
    fontFamily: FONT_FAMILY.REGULAR,
    textAlign: 'center',
    lineHeight: scale(20),
    marginHorizontal: scale(16)
  },
  buttonContainer: {
    flexDirection: 'row',
    marginHorizontal: scale(8),
    marginVertical: scale(16),
    justifyContent: 'center'
  },
  imgContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: DEVICE.WIDTH
  },
  customBtnContainer: {
    width: DEVICE.WIDTH - scale(32),
    marginHorizontal: scale(8),
    borderRadius: scale(12)
  },
  image: {
    width: scale(70),
    height: scale(70)
  }
});

export default styles;
