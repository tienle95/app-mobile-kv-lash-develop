import { Modal, TouchableOpacity, View } from 'react-native';
import React, { forwardRef, useImperativeHandle, useState, useCallback } from 'react';
import styles from './styles';
import { ic_cancel } from 'assets/icons';
import AppText from 'components/AppText';
import FastImage from 'react-native-fast-image';
import AppButton from '../../AppButton';

const ModalNotify = (props, ref) => {
  const {
    image,
    title,
    description,
    textButton,
    onPress,
    cancel,
    onCancel,
    titleCancel,
    style,
    translateDes = true,
    onBack = null,
    wrapperContentStyle
  } = props;

  const [isShow, setIsShow] = useState(false);
  useImperativeHandle(
    ref,
    () => {
      return { hide, show };
    },
    [hide, show]
  );

  const hide = useCallback(() => {
    setIsShow(false);
  }, []);

  const show = useCallback(() => {
    setIsShow(true);
  }, []);

  const onCancelPress = useCallback(() => {
    hide();
    onCancel?.();
  }, [hide, onCancel]);

  const onBacKPress = useCallback(() => {
    hide();
    onBack ? onBack?.() : onCancel?.();
  }, [hide, onBack, onCancel]);

  const onSuccessPress = useCallback(() => {
    hide();
    onPress?.();
  }, [hide, onPress]);

  const descriptionContent = useCallback(() => {
    switch (typeof description) {
      case 'string':
        return (
          <AppText style={styles.description} translate={translateDes}>
            {description}
          </AppText>
        );

      case 'function':
        return <View style={style}>{description()}</View>;
    }
  }, [description, style, translateDes]);

  return (
    <Modal animationType="fade" transparent={true} visible={isShow}>
      <View style={[styles.container, style]}>
        <View style={[styles.content, wrapperContentStyle]}>
          <View style={styles.header}>
            <View />
            <TouchableOpacity onPress={onBacKPress}>
              <FastImage source={ic_cancel} style={styles.icClose} resizeMode="contain" />
            </TouchableOpacity>
          </View>
          <View style={styles.imgContainer}>
            <FastImage source={image} resizeMode="contain" style={styles.image} />
          </View>
          {!!title && (
            <AppText style={styles.title} translate>
              {title}
            </AppText>
          )}
          {!!description && descriptionContent()}
          <View style={styles.buttonContainer}>
            {/* {(!!cancel || !!titleCancel) && (
              <View style={styles.customBtnContainer}>
                <CustomButton
                  isHaveBoxShadow={true}
                  onPress={onCancelPress}
                  title={titleCancel || 'common.cancel'}
                  isDelete={true}
                />
              </View>
            )} */}
            {/* <View style={styles.customBtnContainer}>
              <CustomButton onPress={onSuccessPress} title={textButton} isHaveBoxShadow={true} />
            </View> */}
            <AppButton
              style={styles.customBtnContainer}
              title={textButton}
              onPress={onSuccessPress}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default forwardRef(ModalNotify);
