import AppText from 'components/AppText';
import StyledTouchable from 'components/StyledTouchable';
import React, { useState } from 'react';
import { View } from 'react-native';
import DatePicker from 'react-native-date-picker';
import FastImage from 'react-native-fast-image';
import Modal from 'react-native-modal';
import { ic_close } from '../../../assets/icons';
import { colors } from '../../../constants/colors';
import { scale } from '../../../constants/scale';
import styles from './styles';

const ModalDatePicker = props => {
  const {
    mode = 'date',
    isVisible,
    handleCancel,
    onConfirm,
    value,
    title = 'Choose date',
    maximumDate,
    minimumDate
  } = props;
  const [date, setDate] = useState(new Date());

  const handleConfirm = () => {
    handleCancel();
    onConfirm(date);
  };

  const handleClose = () => {
    if (value) {
      setDate(value);
    }
    handleCancel();
  };

  return (
    <Modal
      isVisible={isVisible}
      backdropTransitionOutTiming={0}
      swipeDirection={'down'}
      onBackdropPress={handleClose}
      onBackButtonPress={handleClose}
      style={styles.containerModal}>
      <View style={styles.container}>
        <View style={styles.containerPicker}>
          <View />
          <AppText bold size="L">
            {title}
          </AppText>
          <StyledTouchable onPress={handleClose}>
            <FastImage
              source={ic_close}
              style={{ width: scale(24), height: scale(24) }}
              resizeMode="contain"
            />
          </StyledTouchable>
        </View>
        <DatePicker
          mode={mode}
          date={date}
          maximumDate={maximumDate}
          minimumDate={minimumDate}
          androidVariant="iosClone"
          onDateChange={date => setDate(date)}
          theme="auto"
          locale="en"
          is24hourSource="locale"
          style={styles.mainDatePicker}
        />
        <View style={styles.btnArea}>
          <StyledTouchable onPress={handleClose} style={styles.btnCancel}>
            <AppText size="L" weight="semi-bold" style={styles.btnTitle} color={colors.white}>
              {'Cancel'}
            </AppText>
          </StyledTouchable>
          <StyledTouchable onPress={handleConfirm} style={styles.btnConfirm}>
            <AppText size="L" weight="semi-bold" style={styles.btnTitle} color={colors.white}>
              {'Choose'}
            </AppText>
          </StyledTouchable>
        </View>
      </View>
    </Modal>
  );
};

export default React.memo(ModalDatePicker);
