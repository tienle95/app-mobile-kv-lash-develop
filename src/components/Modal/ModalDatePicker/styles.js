import { StyleSheet } from 'react-native';
import { DEVICE, scale } from '../../../constants/scale';
import { colors } from '../../../constants/colors';

const styles = StyleSheet.create({
  containerModal: {
    width: '100%',
    alignSelf: 'center',
    justifyContent: 'flex-end',
    margin: 0
  },
  container: {
    backgroundColor: '#fff',
    borderTopEndRadius: scale(12),
    borderTopStartRadius: scale(12),
    width: '100%',
    alignItems: 'center'
  },
  containerPicker: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: scale(10),
    paddingVertical: scale(16)
  },
  btnConfirm: {
    backgroundColor: colors.primary_color,
    height: scale(56),
    width: DEVICE.WIDTH / 2 - scale(20),
    borderRadius: scale(4),
    alignItems: 'center',
    marginBottom: scale(36)
  },
  btnCancel: {
    backgroundColor: colors.disable_color,
    height: scale(56),
    width: DEVICE.WIDTH / 2 - scale(20),
    borderRadius: scale(4),
    alignItems: 'center',
    marginBottom: scale(36)
  },
  btnTitle: {
    lineHeight: scale(56)
  },
  mainDatePicker: {
    height: scale(200)
  },
  btnArea: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: DEVICE.WIDTH - scale(32)
  }
});
export default styles;
