import React from 'react';
import { TouchableOpacity } from 'react-native';
import { throttle } from 'lodash';

const configThrottle = { trailing: false };
const onPressDefault = () => null;

const StyledTouchable = props => {
  const {
    disabled,
    children,
    style,
    throttleTime = 500,
    onPress = onPressDefault,
    activeOpacity = 0.8
  } = props;

  const handlePress = throttle(onPress, throttleTime, configThrottle);

  return (
    <TouchableOpacity
      activeOpacity={activeOpacity}
      disabled={disabled}
      style={style}
      {...props}
      onPress={handlePress}>
      {children}
    </TouchableOpacity>
  );
};

export default StyledTouchable;
