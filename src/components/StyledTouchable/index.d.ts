import React from 'react';
import { StyleProp, TextStyle, TouchableOpacityProps } from 'react-native';

interface StyledTouchableProps extends TouchableOpacityProps {}
declare const StyledTouchable: React.FC<StyledTouchableProps>;

export default StyledTouchable;
