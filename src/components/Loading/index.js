import { ActivityIndicator, StyleSheet, View } from 'react-native';
import React from 'react';
import { DEVICE } from 'constants/scale';
import { colors } from 'constants/colors';

const Loading = () => {
  return (
    <View style={styles.container}>
      <ActivityIndicator color={colors.count_trip} size={'large'} />
    </View>
  );
};

export default React.memo(Loading);

const styles = StyleSheet.create({
  container: {
    height: DEVICE.HEIGHT,
    width: DEVICE.WIDTH,
    backgroundColor: colors.black_opacity,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 9999,
    ...StyleSheet.absoluteFill
  }
});
