import React, { useEffect, useState } from 'react';
import { ActivityIndicator, TouchableOpacity } from 'react-native';
import AppText from '../AppText';
import styles from './styles';
import { colors } from 'constants/colors';

const AppButton = ({
  style,
  title,
  color,
  textColor,
  onPress,
  boldTitle = false,
  numberOfLines,
  adjustsFontSizeToFit,
  disabled,
  isLoading,
  outlined = false,
  children
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[styles.container, { backgroundColor: color }, outlined && styles.outlined, style]}
      disabled={disabled}
      onPress={onPress}>
      {isLoading ? (
        <ActivityIndicator color={colors.white} />
      ) : title ? (
        <AppText
          translate
          color={textColor || colors.white}
          weight={'semi-bold'}
          numberOfLines={numberOfLines}
          adjustsFontSizeToFit={adjustsFontSizeToFit}>
          {title}
        </AppText>
      ) : children ? (
        typeof children === 'string' ? (
          <AppText
            size="L"
            translate
            color={textColor || colors.white}
            bold={boldTitle}
            numberOfLines={numberOfLines}
            adjustsFontSizeToFit={adjustsFontSizeToFit}>
            {children}
          </AppText>
        ) : (
          children
        )
      ) : null}
    </TouchableOpacity>
  );
};

AppButton.defaultProps = {
  style: {},
  title: '',
  color: colors.app_primary,
  textColor: colors.white,
  onPress: () => {},
  numberOfLines: undefined,
  adjustsFontSizeToFit: false,
  disabled: false,
  isLoading: false
};

// AppButton.propTypes = {
//   style: PropTypes.object,
//   title: PropTypes.string,
//   color: PropTypes.string,
//   colorText: PropTypes.string,
//   onPress: PropTypes.func,
//   numberOfLines: PropTypes.number,
//   adjustsFontSizeToFit: PropTypes.bool,
//   disabled: PropTypes.bool,
//   isLoading: PropTypes.bool,
// };

export default React.memo(AppButton);
