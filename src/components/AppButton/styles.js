import { StyleSheet } from 'react-native';
import { scale } from 'constants/scale';
import { colors } from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: scale(8),
    paddingVertical: scale(12)
  },
  outlined: {
    borderWidth: 1,
    backgroundColor: colors.transparent
  }
});

export default styles;
