import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { useDispatch } from 'react-redux';

import { savePreviousCurrentRoute } from '../redux/historyRoute/actions';
import SCREENS from '../constants/screens';
import HomeContainer from '../screens/home/Home.container';
import NavigationServices from '../utils/navigationServices';
import BookingOnlineContainer from 'src/screens/bookingOnline/BookingOnline.container';
import MyAppointmentContainer from 'src/screens/myAppointment/MyAppointment.container';
import WebViewContainer from 'src/screens/webView/WebView.container';
import ContactUsContainer from 'src/screens/contactUs/ContactUs.container';
import NotificationContainer from '../screens/notification/Notification.container';
import NotificationDetailScreen from '../screens/notificationDetail/NotificationDetail.container';
import BookingScreen from '../screens/booking/Booking.container';
import BranchContainer from 'src/screens/branch/Branch.container';

const StackFlow = createStackNavigator();
const BoilerplateStack = props => {
  return (
    <StackFlow.Navigator initialRouteName={SCREENS.HOME}>
      <StackFlow.Screen
        name={SCREENS.HOME}
        component={HomeContainer}
        options={{ headerShown: false }}
      />
      <StackFlow.Screen
        name={SCREENS.BOOKING_ONLINE}
        component={BookingOnlineContainer}
        options={{ headerShown: false }}
      />
      <StackFlow.Screen
        name={SCREENS.MY_APPOINTMENT}
        component={MyAppointmentContainer}
        options={{ headerShown: false }}
      />
      <StackFlow.Screen
        name={SCREENS.WEB_VIEW}
        component={WebViewContainer}
        options={{ headerShown: false }}
      />
      <StackFlow.Screen
        name={SCREENS.CONTACT_US}
        component={ContactUsContainer}
        options={{ headerShown: false }}
      />

      <StackFlow.Screen
        name={SCREENS.BRANCH}
        component={BranchContainer}
        options={{ headerShown: false }}
      />
      <StackFlow.Screen
        name={SCREENS.NOTIFICATION}
        component={NotificationContainer}
        options={{ headerShown: false }}
      />
      <StackFlow.Screen
        name={SCREENS.NOTIFICATION_DETAIL}
        component={NotificationDetailScreen}
        options={{ headerShown: false }}
      />
      <StackFlow.Screen
        name={SCREENS.BOOKING}
        component={BookingScreen}
        options={{ headerShown: false }}
      />
    </StackFlow.Navigator>
  );
};

const AppContainer = props => {
  const routeNameRef = React.useRef();
  const dispatch = useDispatch();

  return (
    <NavigationContainer
      ref={NavigationServices.navigationRef}
      onReady={() => {
        routeNameRef.current = NavigationServices.navigationRef.current.getCurrentRoute().name;
      }}
      onStateChange={state => {
        const previousRouteName = routeNameRef.current;
        const currentRouteName = NavigationServices.navigationRef.current.getCurrentRoute().name;

        dispatch(
          savePreviousCurrentRoute({
            previousRouteName: null,
            currentRouteName: currentRouteName
          })
        );

        if (previousRouteName !== currentRouteName) {
          // The line below uses the expo-firebase-analytics tracker
          // https://docs.expo.io/versions/latest/sdk/firebase-analytics/
          // Change this line to use another Mobile analytics SDK
          // Analytics.setCurrentScreen(currentRouteName);
          //save history route
          // dispatch(
          //   savePreviousCurrentRoute({
          //     previousRouteName: null,
          //     currentRouteName: currentRouteName
          //   })
          // );
        }
      }}>
      <BoilerplateStack {...props} />
    </NavigationContainer>
  );
};

export default AppContainer;
