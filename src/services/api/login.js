import {END_POINTS} from '@constants/apiUrl';
import APIUtils from '@utils/apiUtils';

export function requestOTP({data}) {
  return APIUtils.post(END_POINTS.REQUEST_OTP, {
    body: data,
  });
}
export function sendOTP({data}) {
  return APIUtils.post(END_POINTS.VERIFY_OTP, {
    body: data,
  });
}
export function getUserInfo() {
  return APIUtils.get(END_POINTS.GET_USER_INFO, {method: 'GET'});
}
