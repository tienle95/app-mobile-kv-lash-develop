import { END_POINTS } from '../../constants/apiUrl';
import { buildURLWithParams } from '../../helpers/queryString';
import apiUtils from '../../utils/apiUtils';

export function getNotificationApi(params) {
  return apiUtils.get(buildURLWithParams(END_POINTS.NOTIFICATION, params));
}
