import { END_POINTS } from 'constants/apiUrl';
import apiUtils from 'utils/apiUtils';
import { APIUtils } from '../../utils/axiosClient';

export function getListCandidateApi(action) {
  const { params } = action;
  return apiUtils.post(END_POINTS.GET_LIST_CANDIDATE, { body: params });
}

export function getHomeApi(action) {
  const { params } = action;
  return apiUtils.post(END_POINTS.GET_HOME, { body: params });
}

export function getBookingApi(action) {
  const { params } = action;
  return apiUtils.post(END_POINTS.GET_BOOKING, { body: params });
}

export function getAppointmentApi(action) {
  const { params } = action;
  return apiUtils.post(END_POINTS.GET_APPOINTMENT, { body: params });
}

export function getContactApi(action) {
  const { params } = action;
  return apiUtils.post(END_POINTS.GET_CONTACT, { body: params });
}

export function getIntroduceApi(action) {
  const { params } = action;
  return apiUtils.post(END_POINTS.GET_INTRODUCE, { body: params });
}

export function getBranchApi(action) {
  const { params } = action;
  return apiUtils.get(END_POINTS.GET_BRANCH, { body: params });
}

export function submitBookingApi(data) {
  return APIUtils.post(END_POINTS.BOOKING, data);
}

export function getPopupPromotionApi(action) {
  const { params } = action;
  return apiUtils.get(END_POINTS.GET_POPUP_PROMOTION, { body: params });
}
