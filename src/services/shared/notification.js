import notifee, { AndroidImportance } from '@notifee/react-native';
import SCREENS from '../../constants/screens';
import NavigationServices from '../../utils/navigationServices';

const handleClickNotification = notification => {
  if (notification && notification.data && notification.data.type) {
    switch (+notification.data.type) {
      case 1:
        NavigationServices.navigate(SCREENS.NOTIFICATION_DETAIL);
        break;
      default:
        NavigationServices.navigate(SCREENS.NOTIFICATION);
    }
  }
};

const onNotifeeMessageReceived = async message => {
  console.log('message', message);
  const channelId = await notifee.createChannel({
    id: 'kvlash_notification',
    name: 'KVLash Notifications',
    importance: AndroidImportance.HIGH
  });

  await notifee.displayNotification({
    id: message.messageId,
    title: message.data.title,
    body: message.data.body,
    data: message.data,
    android: {
      channelId: channelId,
      importance: AndroidImportance.HIGH,
      smallIcon: 'ic_notification_small',
      pressAction: {
        id: 'default'
      },
      actions: JSON.parse(message?.data?.actions)
    }
  });
};

export { handleClickNotification, onNotifeeMessageReceived };
