import React, { useEffect } from 'react';

import AppContainer from './navigator';
import messaging from '@react-native-firebase/messaging';
import { useDispatch } from 'react-redux';
import { saveFcmTokenSuccess } from './redux/app/actions';
import { Platform } from 'react-native';

const Root = props => {
  const dispatch = useDispatch();
  useEffect(() => {
    (async () => {
      try {
        if (Platform.OS === 'android') {
          await messaging().registerDeviceForRemoteMessages();
        }
        const token = await messaging().getToken();
        if (token) {
          dispatch(saveFcmTokenSuccess(token));
        }
      } catch (error) {
        console.log('error', error);
      }
    })();
  }, []);
  return <AppContainer {...props} />;
};

export default Root;
