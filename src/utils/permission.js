import {Platform} from 'react-native';
import {check, PERMISSIONS, RESULTS, request} from 'react-native-permissions';

export async function checkLocationPermission() {
  try {
    const permission =
      Platform.OS === 'android'
        ? PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
        : PERMISSIONS.IOS.LOCATION_WHEN_IN_USE;
    let result = await check(permission);

    return result === RESULTS.GRANTED;
  } catch (err) {
    return false;
  }
}

export async function requestLocationPermission() {
  const promise = new Promise((resolve, reject) => {
    const permission =
      Platform.OS === 'android'
        ? PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
        : PERMISSIONS.IOS.LOCATION_WHEN_IN_USE;

    check(permission)
      .then(result => {
        switch (result) {
          case RESULTS.UNAVAILABLE:
            resolve(result);
            break;
          case RESULTS.DENIED:
            request(permission).then(requestPermissionResult => {
              switch (requestPermissionResult) {
                case RESULTS.GRANTED:
                  resolve(requestPermissionResult);
                  break;
                default:
                  resolve(requestPermissionResult);
                  break;
              }
            });
            break;
          case RESULTS.GRANTED:
            resolve(result);
            break;
          case RESULTS.BLOCKED:
            resolve(result);
            break;
        }
      })
      .catch(error => {
        resolve(error);
      });
  });
  return promise;
}
