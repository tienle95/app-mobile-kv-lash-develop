import { img_logoKvlash } from '../../assets/images';

export const NOTIFICATION_DATA = [
  {
    id: 1,
    title: 'KV Lash',
    content: '10% off Discount - The Best Eyelash Extensions in NYC – KV Lash Eyelash Extensions',
    is_read: true,
    created_at: new Date(),
    thumb: img_logoKvlash
  },
  {
    id: 2,
    title: 'KV Lash',
    content: 'Xtreme Lash Extensions: Lash Pass & Black Friday Sale! | kissandmakeupct.com',
    is_read: false,
    created_at: new Date(),
    thumb: img_logoKvlash
  },
  {
    id: 3,
    title: 'KV Lash',
    content: 'Eyelash Extensions $55 Full Set Amazing Offer | Rancho Cucamonga CA',
    is_read: true,
    created_at: new Date(),
    thumb: img_logoKvlash
  },
  {
    id: 4,
    title: 'KV Lash',
    content: 'Lashes Templates Free - Graphic Design Template | VistaCreate',
    is_read: true,
    created_at: new Date(),
    thumb: img_logoKvlash
  }
];
