export const NOTIFICATION_DETAIL_DATA = {
  title: '10% off Discount - The Best Eyelash Extensions in NYC – KV Lash Eyelash Extension',
  date: new Date(),
  content:
    'Log on to your Facebook account, visit, like, and share the Facebook page of one of our locations using hashtag #KvlashEyelash. Bring the proof to your next visit and receive 10% off eyelash services. Take a selfie photo after visiting one of our shops. Your photo must reveal at least 80% of your face. Pst your selfie to your Instagram using hashtag #KvlashEyelash. Bring the proof to your next visit and receive 10% off eyelash services.'
};
