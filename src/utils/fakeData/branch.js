export const BRANCH_DATA = {
  status: 200,
  data: [
    {
      id: 1,
      // image: 'https://iconproducts.com/wp-content/uploads/2018/08/logo-icon-1.jpg',
      title: 'SOUTH SACRAMENTO',
      address: '6825 STOCKTON BLVD STE 250 SACRAMENTO CA 95823'
    },
    {
      id: 2,
      image: 'https://iconproducts.com/wp-content/uploads/2018/08/logo-icon-1.jpg',
      title: 'ELK GROVE CITY',
      address: '8569 BOND ROAD STE 150 ELK GROVE CA 95624'
    },
    {
      id: 3,
      image: 'https://iconproducts.com/wp-content/uploads/2018/08/logo-icon-1.jpg',
      title: 'NORTH SACRAMENTO',
      address: '2020 MARCONI AVENUE SACRAMENTO CA 95821'
    },
    {
      id: 4,
      image: 'https://iconproducts.com/wp-content/uploads/2018/08/logo-icon-1.jpg',
      title: 'SFOLSOM CITY',
      address: '705 E BIDWELL STE 9 FOLSOM CA 95630'
    },
    {
      id: 5,
      image: 'https://iconproducts.com/wp-content/uploads/2018/08/logo-icon-1.jpg',
      title: 'MARCH LANE (STOCKTON CALIFORNIA)',
      address: '1061 E MARCH LANE STE B STOCKTON CA 95210'
    }
  ]
};
