import { colors } from 'constants/colors';
import { ACTION_CODE } from 'constants/string';

const { ic_booking } = require('assets/icons');

export const HOME_DATA = {
  status: 200,
  data: {
    banner: [
      {
        id: 0,
        image: 'https://images.unsplash.com/photo-1607326957431-29d25d2b386f',
        title: 'Dahlia'
      }, // https://unsplash.com/photos/Jup6QMQdLnM
      {
        id: 1,
        image: 'https://images.unsplash.com/photo-1512238701577-f182d9ef8af7',
        title: 'Sunflower'
      }, // https://unsplash.com/photos/oO62CP-g1EA
      {
        id: 2,
        image: 'https://images.unsplash.com/photo-1627522460108-215683bdc9f6',
        title: 'Zinnia'
      }, // https://unsplash.com/photos/gKMmJEvcyA8
      {
        id: 3,
        image: 'https://images.unsplash.com/photo-1587814213271-7a6625b76c33',
        title: 'Tulip'
      }, // https://unsplash.com/photos/N7zBDF1r7PM
      {
        id: 4,
        image: 'https://images.unsplash.com/photo-1588628566587-dbd176de94b4',
        title: 'Chrysanthemum'
      }, // https://unsplash.com/photos/GsGZJMK0bJc
      {
        id: 5,
        image: 'https://images.unsplash.com/photo-1501577316686-a5cbf6c1df7e',
        title: 'Hydrangea'
      } // https://unsplash.com/photos/coIBOiWBPjk
    ],
    button: [
      {
        id: 1,
        title: 'BOOKING ONLINE',
        description: 'description',
        // icon: ic_booking,
        icon: 'https://api.pad-media.com/images/banners/checkicon.png',
        code: ACTION_CODE.BOOKING,
        bgColor: colors.primary_color,
        link: ''
      },
      {
        id: 2,
        title: 'MY APPOINTMENT',
        description: 'description',
        // icon: ic_booking,
        icon: 'https://api.pad-media.com/images/banners/checkicon.png',
        code: ACTION_CODE.APPOINTMENT,
        bgColor: colors.primary_color,
        link: ''
      },
      {
        id: 3,
        title: 'SHOP',
        description: 'description',
        // icon: ic_booking,
        icon: 'https://api.pad-media.com/images/banners/checkicon.png',
        code: ACTION_CODE.SHOP,
        bgColor: colors.primary_color,
        link: 'https://www.kvlash.net/'
      },
      {
        id: 4,
        title: 'GIFT CARD 5% OFF',
        description: 'description',
        // icon: ic_booking,
        icon: 'https://api.pad-media.com/images/banners/checkicon.png',
        code: ACTION_CODE.GIFT_CARD,
        bgColor: colors.primary_color,
        link: 'https://squareup.com/gift/F6FMSRJRS5D5B/order'
      },
      {
        id: 5,
        title: 'CUSTOMER SERVICE',
        description: 'description',
        // icon: ic_booking,
        icon: 'https://api.pad-media.com/images/banners/checkicon.png',
        code: ACTION_CODE.CUSTOMER_SERVICE,
        bgColor: colors.primary_color,
        link: '916.9999989'
      },
      {
        id: 6,
        title: 'CONTACT US',
        description: 'description',
        // icon: ic_booking,
        icon: 'https://api.pad-media.com/images/banners/checkicon.png',
        code: ACTION_CODE.CONTACT_US,
        bgColor: colors.primary_color,
        link: ''
      }
    ]
  }
};

export const BOOKING_DATA = {
  status: 200,
  data: [
    {
      id: 1,
      title: 'SOUTH SACRAMENTO',
      address: '6825 STOCKTON BLVD STE 250 SACRAMENTO CA 95823',
      phone: '916.9999979',
      link: 'https://squareup.com/appointments/book/d9aai6hex24f1o/JXZZY617DNJ3G/services'
    },
    {
      id: 2,
      title: 'SOUTH SACRAMENTO',
      address: '6825 STOCKTON BLVD STE 250 SACRAMENTO CA 95823',
      phone: '916.9999979',
      link: 'https://squareup.com/appointments/book/d9aai6hex24f1o/ETRASVCKE7YXD/services'
    },
    {
      id: 3,
      title: 'SOUTH SACRAMENTO',
      address: '6825 STOCKTON BLVD STE 250 SACRAMENTO CA 95823',
      phone: '916.9999979',
      link: 'https://squareup.com/appointments/book/d9aai6hex24f1o/D6V2J83J6TDMZ/services'
    },
    {
      id: 4,
      title: 'SOUTH SACRAMENTO',
      address: '6825 STOCKTON BLVD STE 250 SACRAMENTO CA 95823',
      phone: '916.9999979',
      link: 'https://squareup.com/appointments/book/d9aai6hex24f1o/LFFVDH2CDXNR9/services'
    },
    {
      id: 5,
      title: 'SOUTH SACRAMENTO',
      address: '6825 STOCKTON BLVD STE 250 SACRAMENTO CA 95823',
      phone: '916.9999979',
      link: 'https://squareup.com/appointments/book/d9aai6hex24f1o/L3RHDHM3FYG2Y/services'
    }
  ]
};

export const CONTACT_DATA = {
  status: 200,
  data: [
    {
      id: 1,
      title: 'PHONE',
      type: 'phone',
      value: '916.9999979'
    },
    {
      id: 2,
      title: 'EMAIL',
      type: 'email',
      value: 'kkvlashstudio@gmail.com'
    }
  ]
};
