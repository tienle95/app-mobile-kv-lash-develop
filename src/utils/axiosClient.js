/* eslint-disable no-undef */
import axios from 'axios';
import cloneDeep from 'lodash/cloneDeep';
import { store } from '../redux/configureStore';
// import { setLoading } from '~redux/system/system.action';

const REQUEST_TIMEOUT = 30000;

const axiosClient = axios.create({
  headers: {
    'Content-Type': 'application/json'
  },
  timeout: REQUEST_TIMEOUT
});

const getToken = () => {
  const token = store.getState().authReducer.accessToken;
  if (token) {
    return token;
  }
  return null;
};

axiosClient.interceptors.request.use(
  async config => {
    let token = getToken();
    if (token) {
      config.headers.Authorization = 'Bearer ' + token;
    }
    if (__DEV__) {
      console.log('>>>>>request>>>>> ', { url: config.url, config: config });
    }
    config.cancelToken = new axios.CancelToken(cancelRequest => {
      setTimeout(() => cancelRequest(`Timeout of ${REQUEST_TIMEOUT}ms.`), REQUEST_TIMEOUT);
    });
    return config;
  },
  err => {
    return Promise.reject(err);
  }
);

axiosClient.interceptors.response.use(
  function (response) {
    if (__DEV__) {
      console.log('>>>>>response>>>>> ', {
        url: response.config.url,
        status: response.status,
        response
      });
    }
    // if (response?.data) {
    //   return response?.data;
    // }
    return response;
  },
  async function (error) {
    console.log('errror ===>', error);
    // connectionChecker(error?.code);
    // let originalRequest = cloneDeep(error.config);
    // console.log('originalRequest', originalRequest);
    // originalRequest._initConfig = originalRequest._initConfig || cloneDeep(error.config);
    // originalRequest._initConfig._retryCount = originalRequest._retryCount || 0;
    // originalRequest._retryCount = originalRequest._retryCount || 0;

    // if (error.response.status === 401 && originalRequest._retryCount >= 3) {
    //   originalRequest._retry = false;
    //   // store.dispatch(logoutHandler());
    //   // NavigationServices.navigate(ROUTE_NAME.MAIN_TAB, {
    //   //   screen: ROUTE_NAME.HOME
    //   // });
    //   return Promise.reject(error);
    // }

    // /**
    //  * Todo handle call refresh token
    //  */
    // if (error.response.status === 401 && (originalRequest._retryCount || 0) < 3) {
    //   originalRequest._retry = true;
    //   originalRequest._initConfig._retryCount += 1;
    //   // call refresh token
    //   // await _refreshToken();
    //   return axiosClient(originalRequest._initConfig);
    // }
    return Promise.reject(error?.response?.data || error);
  }
);

export class APIUtils {
  static get(uri, params, isLoading = true) {
    // const abort = axios.CancelToken.source();
    // const id = setTimeout(
    //   () => abort.cancel(`Timeout of ${REQUEST_TIMEOUT}ms.`),
    //   REQUEST_TIMEOUT,
    // );
    return new Promise((resolve, reject) => {
      axiosClient
        .get(uri, { params })
        .then(response => {
          // clearTimeout(id);
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  static getWithoutAcceptText(uri, params, headers) {
    return new Promise((resolve, reject) =>
      axiosClient
        .get(uri, {
          headers: {
            'Content-Type': 'application/json',
            Accept: '*/*',
            ...headers
          },
          timeout: REQUEST_TIMEOUT,
          params
        })
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        })
    );
  }

  static post(uri, postData, isLoading = true, headers) {
    return new Promise((resolve, reject) => {
      axiosClient
        .post(uri, postData, {
          timeout: REQUEST_TIMEOUT,
          headers: {
            'Content-Type': 'application/json',
            ...headers
          }
        })
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  static delete(uri, deleteBody, headers) {
    return new Promise((resolve, reject) => {
      axiosClient
        .delete(uri, {
          timeout: REQUEST_TIMEOUT,
          headers: {
            'Content-Type': 'application/json',
            ...headers
          },
          data: deleteBody
        })
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          console.log('errr 4', { err });
          reject(err);
        });
    });
  }

  static postFormData(uri, postData, headers) {
    return new Promise((resolve, reject) => {
      axiosClient
        .post(uri, postData, {
          timeout: REQUEST_TIMEOUT,
          headers: {
            'Content-Type': 'multipart/form-data',
            ...headers
          }
        })
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  static put(uri, updateData) {
    return new Promise((resolve, reject) =>
      axiosClient
        .put(uri, updateData, {
          headers: {
            'Content-Type': 'application/json'
          },
          timeout: REQUEST_TIMEOUT
        })
        .then(response => {
          console.log('responsePut', response);
          resolve(response);
        })
        .catch(err => {
          // console.log('[error]', { err });
          reject(err);
        })
    );
  }

  static patch(uri, updateData) {
    return new Promise((resolve, reject) =>
      axiosClient
        .patch(uri, updateData, {
          headers: {
            'Content-Type': 'application/json'
          },
          timeout: REQUEST_TIMEOUT
        })
        .then(response => {
          console.log('responsePatch', response);
          resolve(response);
        })
        .catch(err => {
          // console.log('[error]', { err });
          reject(err);
        })
    );
  }

  static getMultiple(listGetRequest) {
    return new Promise((resolve, reject) => {
      axiosClient
        .all(listGetRequest)
        .then(
          axios.spread((...responses) => {
            resolve(responses);
          })
        )
        .catch(errors => {
          reject(errors);
        });
    });
  }

  // static postUrlencoded = (uri, postData, headers) => {
  //   let config = {
  //     method: 'post',
  //     url: uri,
  //     headers: {
  //       'Content-Type': 'application/x-www-form-urlencoded',
  //     },
  //     data: qs.stringify(postData),
  //   };

  //   return new Promise((resolve, reject) => {
  //     axios(config)
  //       .then(response => {
  //         console.log('postUrlencoded', response);
  //         resolve(response);
  //       })
  //       .catch(err => {
  //         //  console.log('[error]', { err });
  //         console.log('errr 3', { err });
  //         reject(err);
  //       });
  //   });
  // };
}
export default axiosClient;
