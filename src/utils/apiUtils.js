/* eslint-disable no-empty */
/* eslint-disable no-undef */
import fetchIntercept from 'fetch-intercept';
const REQUEST_TIMEOUT = 60000;
import { getToken } from 'src/helpers/tokenHelper';

const refreshToken = async () => {
  // TODO: use token to integrate api

  const token = await getToken();
  if (token) return token;
  return '';
};

const unregister = fetchIntercept.register({
  request: async function (url, config) {
    const token = await refreshToken();

    console.log('token >>>', token);

    const tempConfig = { ...config };
    if (!tempConfig.headers) {
      tempConfig.headers = {};
    }
    if (token && tempConfig && tempConfig.headers && !tempConfig.headers.Authorization) {
      tempConfig.headers.Authorization = 'Bearer ' + token;
    }
    // Modify the url or config here
    return [url, tempConfig];
  },

  requestError: function (error) {
    // Called when an error occured during another 'request' interceptor call
    return Promise.reject(error);
  },

  response: function (response) {
    // Modify the reponse object
    return response;
  },

  responseError: function (error) {
    // Handle an fetch error
    return Promise.reject(error);
  }
});

class APIUtils {
  additionalHeader = {};

  #buildURLWithParams = (url, tmpParams = {}) => {
    let params = { ...tmpParams };
    let requestedURL = url;
    if (params) {
      const keys = Object.keys(params);

      if (Array.isArray(keys) && keys.length > 0) {
        requestedURL += '?';
        for (var property of keys) {
          const index = keys.indexOf(property);
          if (index > 0 && index < keys.length) {
            requestedURL += '&';
          }
          requestedURL += `${property}=${params[property]}`;
        }
      }
    }
    return requestedURL;
  };

  #handleRequest = (url, config) => {
    // eslint-disable-next-line no-undef
    const controller = new AbortController();
    const signal = controller.signal;

    const fetchConfig = {
      ...config,
      signal
    };

    setTimeout(() => {
      controller.abort();
    }, REQUEST_TIMEOUT);
    if (__DEV__) {
      console.log('>>>>>request>>>>>', {
        url,
        config: fetchConfig
      });
    }

    return fetch(url, fetchConfig).then(async response => {
      let responseJson = {};
      try {
        responseJson = await response.json();
      } catch (err) {}
      if (__DEV__) {
        console.log('>>>>>response>>>>>', {
          url,
          data: responseJson,
          status: response.status
        });
      }
      if (response.status >= 400 && response.status <= 499) {
        throw { data: responseJson, status: response.status };
      }
      if (response.status >= 200 && response.status <= 299) {
        return { data: responseJson, status: response.status };
      }
      throw { data: responseJson, status: response.status };
    });
  };

  #buildConfig = config => {
    let headersDefault = {
      'Content-Type': 'application/json',
      Accept: 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      e_platform: 'mobile'
    };
    const { headers, params, method, body, ...restConfig } = config;

    return {
      cache: 'default',
      credentials: 'include',
      headers: {
        ...headersDefault,
        ...config.headers,
        ...this.additionalHeader
      },
      ...restConfig,
      method,
      body: method !== 'GET' && method !== 'DELETE' ? JSON.stringify(config.body) : undefined
    };
  };

  setAdditionalHeader = data => {
    this.additionalHeader = data;
  };

  get(url, config = { headers: {}, params: {} }) {
    const requestedURL = this.#buildURLWithParams(url, config?.params);
    const fetchConfig = this.#buildConfig({ ...config, method: 'GET' });
    return this.#handleRequest(requestedURL, fetchConfig);
  }

  post(url, config = { headers: {} }) {
    const fetchConfig = this.#buildConfig({ ...config, method: 'POST' });
    return this.#handleRequest(url, fetchConfig);
  }

  delete(url, config = { headers: {} }) {
    const fetchConfig = this.#buildConfig({ ...config, method: 'DELETE' });
    return this.#handleRequest(url, fetchConfig);
  }

  put(url, config = { headers: {} }) {
    const fetchConfig = this.#buildConfig({ ...config, method: 'PUT' });
    return this.#handleRequest(url, fetchConfig);
  }
}

const apiUtils = new APIUtils();

export default apiUtils;
