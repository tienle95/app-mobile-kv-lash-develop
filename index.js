/**
 * @format
 */

import { AppRegistry, Platform } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import notifee, { EventType } from '@notifee/react-native';
import {
  handleClickNotification,
  onNotifeeMessageReceived
} from './src/services/shared/notification';
import messaging from '@react-native-firebase/messaging';

messaging().setBackgroundMessageHandler(async message => {
  console.log('setBackgroundMessageHandler', message);
  if (Platform.OS === 'ios') {
    await onNotifeeMessageReceived(message);
  }
});

notifee.onBackgroundEvent(async ({ type, detail }) => {
  switch (type) {
    case EventType.DISMISSED:
      await notifee.cancelNotification(detail.notification.id);
      break;
    case EventType.PRESS:
      if (detail?.pressAction === 'mark_as_read') {
        console.log('mark as read');
        await notifee.cancelNotification(detail.notification.id);
      } else {
        handleClickNotification(detail.notification);
      }
      break;
    default:
      break;
  }
});

AppRegistry.registerComponent(appName, () => App);
